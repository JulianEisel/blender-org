<?php
/**
 * Plugin Name:  Posts & RSS Shortcode
 * Description:  Shortcodes for displaying WordPress posts and RSS
 * Author:       Rene Morozowich, Pablo Vazquez
 * Version:      1.1
 * Text Domain:  getblogposts
 * License:      GPL v2 or later
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package getblogposts
 */

/*
 * Original code taken from this nice post by Rene Morozowich
 * https://renemorozowich.com/using-wordpress-rest-api-get-blogs/
*/

// Disable direct file access.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Get posts via REST API.
 */
function get_posts_as_cards($atts) {

	/*
	 * Shortcode parameters:
	 * site: for WordPress sites it must be the root URL, e.g. 'https://code.blender.org/', or 'https://www.blender.org/'
	 * per_page: how many posts to show
	 * categories: filter by category
	 *
	 * Example shortcode:
	 * [get_posts_as_cards site="https://code.blender.org/" per_page="4" categories="70"]
	 * [get_posts_as_cards site="studio" per_page="4" utm_medium="news"]
	 */

	$allposts = '';
	$i = 0;

	$defaults = array(
		"site"				=> '',
		"per_page"		=> '4',
		"categories"	=> '70',
		"utm_medium"	=> 'homepage'
	);

	extract(shortcode_atts($defaults, $atts));

	$params = array(
		"site"				=> $site,
		"per_page"		=> $per_page,
		"categories"	=> $categories,
		"utm_medium"	=> $utm_medium
	);

	/* Custom feed treatment for Blender Studio. */
	if ($site == 'studio') {
		$url = 'https://studio.blender.org/blog/latest/feed/';
		$xml = simplexml_load_file($url);

		$posts = $xml->channel->item;

	} else {
		/* WordPress sites. */
		$url = $site . 'wp-json/wp/v2/posts?_embed&' . http_build_query($params);
		$response = wp_remote_get($url);

		if ( is_wp_error( $response ) ) { return; }

		$posts = json_decode( wp_remote_retrieve_body( $response ), true );
	}

	// Exit if nothing is returned.
	if (empty($posts)) {return;}

	// If there are posts.
	if ( ! empty( $posts ) ) {

		// For each post.
		foreach ( $posts as $post ) {

			if ($site == 'studio') {
				$item = array(
						'title' => (string) esc_html($post->title),
						'link' => (string) esc_url($post->link),
						'description' => (string) $post->description,
						'thumbnail' => (string)$post->enclosure['url']
				);
			} else {
				$item = array(
						'title' => (string) esc_html( $post['title']['rendered'] ),
						'link' => (string) esc_url( $post['link'] ),
						'description' => (string) $post['excerpt']['rendered'],
						'thumbnail' => (string) $post['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['post-thumbnail']['source_url']
				);
			}

			// Build HTML.
			$allposts .= '<div class="cards-list-item-outer">';
			$allposts .= '<div class="cards-list-item-inner">';

			if ($item['thumbnail']) {
				$allposts .= '<a href="' . $item['link'] . '?utm_medium=' . $utm_medium . '" target="_blank" class="cards-list-item-thumbnail">';
				$allposts .= '<img src="' . $item['thumbnail'] . '" alt="' . $item['title'] . '">';
				$allposts .= '</a>';
			}

			$allposts .= '<a href="' . $item['link'] . '?utm_medium=' . $utm_medium . '" target="_blank" class="cards-list-item-title">' . $item['title'] . '</a>';
			$allposts .= '<a href="' . $item['link'] . '?utm_medium=' . $utm_medium . '" target="_blank" class="cards-list-item-excerpt">' . $item['description'] . '</a>';
			$allposts .= '</div>';
			$allposts .= '</div>';

			if (++$i == $per_page) break;
		}

		return $allposts;
	}
}

// Register as a shortcode to be used on the site.
add_shortcode('get_posts_as_cards', 'get_posts_as_cards');


/*
 * Original code taken from the WordPress reference page for fetch_feed()
 * https://developer.wordpress.org/reference/functions/fetch_feed/#comment-681
*/

function get_rss_as_list( $atts ) {
	extract(shortcode_atts(array(
		"feed" 		=> '',
		"num" 		=> '5',
		"excerpt" => true,
		"date" 		=> false,
		"target"	=> '_self'
	), $atts));

	require_once(ABSPATH.WPINC.'/class-simplepie.php');

	include_once( ABSPATH . WPINC . '/feed.php' );

	// Get a SimplePie feed object from the specified feed source.
	$rss = fetch_feed($feed);
	$maxitems = 0;

	if (! is_wp_error($rss)):
		// Figure out how many total items there are, but limit it to the shortcode argument.
		$maxitems = $rss->get_item_quantity($num);
		$rss_items = $rss->get_items( 0, $maxitems );
	endif;

	$content = '<ul>';

	if ($maxitems == 0){
		$content .= '<li>No items to show.</li>';
	} else{
		foreach ( $rss_items as $item ){
			$content .= '<li>';

			$content .= '<a href="' . $item->get_permalink() . '"';
			if ($target){
				$content .= ' target="' . $target . '" ';
			}
			$content .= '>';
			$content .= $item->get_title();
			$content .= '</a>';

			if ($date == 'true'){
				$content .= '<small class="text-muted d-block">' . $item->get_date() . '</small>';
			}

			$content .= '</li>';
		}
	}

	$content .= '</ul>';

	return $content;
}

add_shortcode('rss', 'get_rss_as_list');

?>
