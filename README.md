# blender.org WordPress Theme

'bthree' is the WordPress theme of blender.org's main website. We invite web developers to help and provide fixes.

Reporting issues and pull requests are welcome in the [project page](https://projects.blender.org/infrastructure/blender-org).

## Installation

1. Install [WordPress](https://wordpress.org/documentation/article/installing-wordpress-on-your-own-computer/) locally (use `blender.local` as site URL)
2. Delete the `wp-content` folder
3. `git clone` this repository as a `wp-content` folder
4. `cd wp-content`
5. `git submodule init`
6. `git submodule update`
7. `npm install`
8. `npm run build`

To develop, you can use `npm start` to open `blender.local` in the browser and watch for local file changes.

That should get the site running. Optionally, you can populate it with the following database and uploads:
* [Database dump](https://www.dropbox.com/s/vdf71y187f8khip/wp_www.sql.gz?dl=1) (user: `admin`, password: `password`)
* [Uploads folder](https://www.dropbox.com/s/ddm3rzdlgyum6pn/uploads.zip?dl=1), move the `upload` folder into `wp-content`

## Required Plugins
- [ACF Pro](http://www.advancedcustomfields.com/)

## Installation Notes
### Docker
If you choose to install WordPress via Docker, the `volumes` section is needed to override `wp-config.php` and the entire `wp-content` folder.

Example `docker-compose.yaml`:
```
  wordpress:
    image: wordpress
    container_name:          wordpress
    restart: always
    ports:
      - 8080:80
    volumes:
      - /home/<local_install_path>/wordpress/wp-content:/var/www/html/wp-content
      - /home/<local_install_path>/wordpress/wp-config.php:/var/www/html/wp-config.php
```

Remember to set the user:group (so you can e.g. update plugins):

1. `docker exec -ti wordpress bash`
2. `chown -R www-data:www-data /var/www`

### Apache
If you choose to install WordPress using Apache, you might want to check these settings:

1. Enable short_code (in php.ini)  - this allows code to run with "<?" as well as <?php". The template and plugins use both formats.
2. Enable "rewrite_module" or "mod_rewrite" - this allow the links to work since the permalink format is different than the default.

## Production Deploy Playbook

- Login to blender.org as borg
- `cd /data/www/vhosts/code.blender.org/wordpress/wp-content/themes/`
- `git pull`
- `sudo apachectl graceful`
- Logout
- Additionally, to push assets, run `./deploy.sh blender.org`
