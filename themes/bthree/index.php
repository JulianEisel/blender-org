<?php get_header(); ?>
<?php get_header('static'); ?>

<div class="container">

	<?php if (is_home()): ?>
	<?php dynamic_sidebar('posts-index-header'); ?>
	<?php endif; ?>

	<?php if (is_search()): ?>
		<?php
		global $wp_query;
		$results_count = $wp_query->found_posts;
		$results_label = $wp_query->found_posts > 1 ? 'results' : 'result';
		?>
		<h1 class="my-4">
			<span class="text-secondary"><?=$results_count?> <?=$results_label?> for</span> <?php the_search_query(); ?>
		</h1>
	<?php endif; ?>

	<?php if (is_archive()): ?>
		<div class="mt-5 mb-3">
			<?php if (is_category()): ?>
				<h2>Posts under <?php the_category(', '); ?></h2>
				<?php echo(category_description()); ?>
			<?php endif; ?>

			<?php if (is_author()): ?>
				<h2>Posts by <?php the_author(); ?></h2>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<div class="cards-list card-layout-card-transparent">
	<?php if (have_posts()){ ?>
		<?php while ( have_posts() ) : the_post(); ?>

		<?php
			$post_author = get_the_author();
			$post_author_url = get_author_posts_url(get_the_author_meta('id'));
			$post_status = get_post_status();

			$is_guest_blogpost = get_field('is_guest_blog_post');
			if ($is_guest_blogpost) {
				$guest_author = get_field('guest_author');

				$post_author     = $guest_author['author_name'];
				$post_author_url = $guest_author['author_url'];
			}
		?>

		<div class="cards-list-item-outer">
			<div class="cards-list-item-inner">
				<?php if (has_post_thumbnail()): ?>
					<a href="<?php the_permalink(); ?>" class="cards-list-item-thumbnail">
						<?php the_post_thumbnail('featured-list', ['class' => 'img-fluid', 'title' => get_the_title()]); ?>
					</a>
				<?php endif; ?>
				<a class="cards-list-item-title" href="<?php the_permalink(); ?>"><?php the_title() ?></a>
				<a class="cards-list-item-excerpt" href="<?php the_permalink(); ?>"><?php the_excerpt();?></a>
				<div class="cards-list-item-extra">
					<ul>
						<li>
							<a href="<?php the_permalink(); ?>">
								<?php the_time('M j'); ?><?=((get_the_time('Y') == date('Y')) ? '' : the_time(', Y'))?>
							</a>
						</li>

						<li>
						<?php if ($is_guest_blogpost): ?>
							<?=$post_author?>
						<?php else: ?>
							<?php the_author_posts_link(); ?>
						<?php endif; ?>
						</li>

						<?php if ($post_status != 'publish'): ?>
						<li title="Status" class="text-warning"><?=$post_status?></li>
						<?php endif; ?>

						<?php if (!empty($post->post_password)): ?>
						<li class="text-danger">PASSWORD PROTECTED</li>
						<?php endif; ?>

						<?php if (get_comments_number()): ?>
							<li class="right"><?php comments_popup_link('', '1 <i class="i-comment"></i>', '% <i class="i-comment"></i>'); ?></li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>

	<?php endwhile; ?>

	<?php } else { ?>
		<h2>Nothing to see here.</h2>
	<?php }; ?>
	</div>

	<div class="blog-navigation single posts <?=(is_paged() ? '' : 'is-first-page')?>">
		<?php posts_nav_link(' ', '<span>NEWER POSTS</span> <i class="i-chevron-right"></i>', '<i class="i-chevron-left"></i> <span>OLDER POSTS</span>' ); ?>
	</div>
</div>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
