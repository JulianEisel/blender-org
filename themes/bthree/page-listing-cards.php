<?php
/*
Template Name: Layout: Listing Cards (deprecated)
*/
?>
<?php get_header(); ?>
<?php get_header('static'); ?>

<div class="container py-4">
	<?php while ( have_posts() ) : the_post(); ?>

		<?php the_content(); ?>

		<?php
			$i = 0;
			while( have_rows('section') ): the_row(); ?>
			<?php

				$section_enabled    = get_sub_field('section_enabled');
				$section_title      = get_sub_field('section_title');

				$items_list         = get_sub_field('tutorials_list');
				$cards_per_row      = get_sub_field('cards_per_row');
				$card_layout        = get_sub_field('card_layout');
				$cards_aspect_ratio = get_sub_field('card_aspect_ratio');
				?>

				<?php
				// If the section is enabled, draw the box and title
				if ($section_enabled): ?>
				<section id="section-<?=$i?>">
					<style>
						#section-<?=$i?> > .cards-list {
								--cards-list-items-per-row: <?=($cards_per_row ? $cards_per_row : '4')?>;
							}
					</style>

					<?php if ($section_title): ?>
						<a id="<?=slugify($section_title)?>" href="#<?=slugify($section_title)?>" class="d-block pt-5 pb-4 text-dark">
							<h2><?=$section_title?></h2>
						</a>
					<?php endif; ?>

					<div class="cards-list card-layout-<?=$card_layout?> card-aspect-ratio-<?=$cards_aspect_ratio?>">
						<?php
						// Let's list the listing now
						foreach ($items_list as $list_item):

							// Gather the info
							$list_item_enabled    = $list_item['tutorial_enabled'];
							$list_item_url        = $list_item['tutorial_url'];
							$list_item_title      = $list_item['tutorial_title'];
							$list_item_text       = $list_item['tutorial_description'];
							$list_item_thumbnail  = $list_item['tutorial_thumbnail'];

							$list_item_image_size_default   = 'thumbnail_card';
							$list_item_image_size = (array_key_exists('image_size', $list_item) ? $list_item['image_size'] : $list_item_image_size_default);

							// If it hasn't been selected yet.
							if (is_array($list_item_image_size)){
								$list_item_image_size = $list_item_image_size_default;
							}

							if ($list_item_enabled): ?>
							<div class="cards-list-item-outer">
								<div class="cards-list-item-inner">

									<?php if ($list_item_thumbnail && $list_item_url): ?>
										<a href="<?=$list_item_url?>" class="cards-list-item-thumbnail">
											<img src="<?=$list_item_thumbnail['sizes'][$list_item_image_size]?>" alt="<?=$list_item_title?>">
										</a>
									<?php elseif ($list_item_thumbnail): ?>
										<div class="cards-list-item-thumbnail">
											<img src="<?=$list_item_thumbnail['sizes'][$list_item_image_size]?>" alt="<?=$list_item_title?>">
										</div>
									<?php endif; ?>

									<div class="cards-list-item-info">
										<?php if ($list_item_title && $list_item_url): ?>
											<a class="cards-list-item-title" href="<?=$list_item_url?>"><?=$list_item_title?></a>
										<?php elseif ($list_item_title): ?>
											<span class="cards-list-item-title"><?=$list_item_title?></span>
										<?php endif; ?>

										<?php if ($list_item_text): ?>
											<span class="cards-list-item-excerpt" href="<?=$list_item_url?>"><?=$list_item_text?></span>
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php endif; // tutorial_enabled ?>
						<?php endforeach; // items_list ?>

					</div>
				</section>
				<?php endif; // section_enabled ?>
		<?php
			$i++;
			endwhile; // sections ?>
	<?php endwhile; // sections ?>
</div>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('a[stats_item]').click(function(e){
			e.preventDefault();
			exit_link_dst = $(this).attr('stats_item');
			ga('send', 'event', 'link', 'exit_item', exit_link_dst);
			window.location = $(this).attr('href');
		});
	});
</script>
