<?php
/*
Template Name: Community
*/
?>

<?php get_header(); ?>
<?php get_header('static'); ?>


<div class="container community">

	<div class="community-group-list">
		<?php

		// Get all the language groups (EN, ES, FR, IT, etc...)
		$language_groups = get_field('language_group');

		if ($language_groups) :
		?>
			<div class="community-languages-list">
				<?php
				foreach ($language_groups as $l) : ?>
					<a class="btn" href="#<?= $l['language']['value'] ?>"><?= $l['language']['label'] ?></a>
				<?php
				endforeach;
				?>
			</div>

			<?php
			while (have_rows('language_group')) : the_row();

				$language       = get_sub_field('language'); // Language Name
				$community      = get_sub_field('community'); // Communities for that language

				$language_iso   = $language['value'];
				$language_label = $language['label'];
			?>
				<div id="<?= $language_iso ?>" class="community-group <?= $language_iso ?>">
					<h3 class="community-language">
						<a href="#<?= $language_iso ?>"><?= $language_label ?></a>
					</h3>

					<div class="cards-list">
						<?php

						if ($community) :
							while (have_rows('community')) : the_row();

								$name = get_sub_field('name'); // Community Name
								$description = get_sub_field('description'); // Community description
								$url = get_sub_field('url'); // Community url
								$url_pretty = pretty_url($url);
								$thumbnail = get_sub_field('thumbnail'); // Community thumbnail

						?>
								<div class="cards-list-item-outer">
									<a class="cards-list-item-inner" href="<?= $url ?>" target="_blank" rel="nofollow">
										<?php if ($thumbnail) : ?>
											<div class="cards-list-item-thumbnail">
												<img src="<?= $thumbnail['sizes']['square'] ?>" alt="<?= $name ?>" />
											</div>
										<?php endif; ?>
										<div class="cards-list-item-details">
											<div class="cards-list-item-title"><?= $name ?></div>
											<div class="cards-list-item-excerpt"><?= $description ?></div>
											<div class="cards-list-item-url">
												<?= $url_pretty ?>
											</div>
										</div>
									</a>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>

			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="text-center text-muted mt-4">
		<small>
			Is your community missing?<br />
			Send your community name, logo and short description to <a href="https://blender.chat/direct/pablovazquez">@pablovazquez on blender.chat</a>
		</small>
	</div>
</div>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
<script>
	var lang = navigator.languages ? navigator.languages[0] : (navigator.language || navigator.userLanguage);

	if (lang) {
		lang = lang.substring(0, 2);
		$('.community-group.' + lang).addClass('active');
	}
</script>
