<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<title><?=get_site_title()?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?=get_shareable_description()?>" >

		<?php if (function_exists('get_field')): ?>
			<?php if( get_field('meta_keywords') ): ?>
			<meta name="keywords" content="<?php the_field('meta_keywords'); ?>">
			<?php endif; ?>
		<?php endif; ?>

		<?php
			/* Do not cache the thanks page, it contains dynamic content. */
			if (is_page_template('page-thanks.php')): ?>
			<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
			<meta http-equiv="Pragma" content="no-cache">
			<meta http-equiv="Expires" content="0">
		<?php endif; ?>

		<meta name="author" content="Blender Foundation">
		<meta name="theme-color" content="#ed8f31">
		<meta property="og:site_name" content="<?=get_bloginfo('name');?>" >
		<meta property="og:title" content="<?=get_site_title()?>" >
		<meta property="og:description" content="<?=get_shareable_description()?>">
		<meta property="og:url" content="<?=the_permalink();?>">
		<meta property="og:image" content="<?=get_shareable_image()?>">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@blender">
		<meta name="twitter:creator" content="@blender">
		<meta name="twitter:title" content="<?=get_site_title()?>">
		<meta name="twitter:description" content="<?=get_shareable_description()?>">
		<meta name="twitter:image" content="<?=get_shareable_image()?>">
		<meta name="twitter:image:alt" content="<?=get_shareable_description()?>">

		<link rel="stylesheet" href="<?=get_stylesheet_uri()?>" type="text/css" media="screen" />
		<link rel="apple-touch-icon" sizes="180x180" href="<?=get_stylesheet_directory_uri()?>/assets/icons/apple-touch-icon.png">
		<link rel="icon" type="image/svg+xml" href="<?=get_stylesheet_directory_uri()?>/assets/icons/favicon.svg">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=get_stylesheet_directory_uri()?>/assets/icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=get_stylesheet_directory_uri()?>/assets/icons/favicon-16x16.png">

		<?php wp_head(); ?>
	</head>

	<?php
		$page_alert          = get_field('page_alert');
		$comments_moderation = wp_count_comments()->moderated;
		$can_see_admin       = current_user_can('edit_posts');
		$hide_navigation     = get_field('navigation_hide');
	?>

	<body class="<?=get_class_body()?><?=($page_alert ? ' with-alert' : '')?>">
		<?php if (is_code_blog()) { include '_navbar_developer.php'; }?>

		<div class="container-main">

		<?php
			global $user_ID;
			if($user_ID && ($can_see_admin)) {
				echo '<div class="whoosh-container">';

				/* Go to /admin. */
				echo '<a href="' . get_admin_url() . '" class="whoosh" title="Admin"><i class="i-settings"></i></a>';

				/* Edit post/page button.*/
				if (is_singular()) {
					edit_post_link('<i class="i-edit" title="Edit this page"></i>', '', '', null, 'whoosh whoosh-page');
				}

				/* See comments, with pending comments counter (only the Code blog has comments). */
				if (is_code_blog() && current_user_can('moderate_comments')){
					echo
						'<a href="' . get_admin_url(null, 'edit-comments.php') . '"' .
						'class="whoosh whoosh-comments ' . ($comments_moderation ? 'has-comments' : '') . '"' .
						'data-comments-count="' . $comments_moderation . '"' .
						'title="Comments"><i class="i-comment"></i>'.
						'</a>';
				}

				echo '</div>';
			}
		?>

		<?php
		/* Main navigation. */
		if (!is_code_blog()): ?>
		<nav class="navbar navbar-expand-md navbar-primary" role="navigation">
			<div class="container">
				<a class="navbar-brand navbar-logo-blender" href="https://www.blender.org" title="Go to blender.org"></a>

				<button class="navbar-mobile-toggler js-show-toggle" data-toggle-menu-id="navbar-mobile">
					<span class="sr-only">Toggle navigation</span>
					<span class="navbar-toggler-icon"><i class="i-menu"></i></span>
				</button>

				<?php nav_menu_primary(); ?>
			</div>
		</nav>
		<?php endif; ?>

		<?php /* Second level navigation. */ ?>
		<?=($hide_navigation ? '' : nav_bar_secondary() )?>

		<?php /* Main navigation on mobile. */ ?>
		<nav id="navbar-mobile" class="navbar-mobile">
			<?php nav_menu_primary(); ?>

			<?php /* Second level navigation on mobile. */?>
			<?=($hide_navigation ? '' : nav_bar_secondary() )?>
		</nav>

	<?php
		/* Page alerts. e.g. "Release notes in progress." */
		$page_alert_text     = get_field('page_alert_text');
		$page_alert_category = get_field('page_alert_category');

		if ($page_alert):
	?>
		<div class="alert alert-<?=($page_alert_category ? $page_alert_category : 'info')?> text-center mb-0">
			<span><?=$page_alert_text?></span>
		</div>
	<?php endif; ?>
