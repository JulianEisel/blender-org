<?php

// Disable XML-RPC.
add_filter('xmlrpc_enabled', '__return_false');


// Menu Registration.
add_action('init', 'register_menus');
function register_menus() {
  register_nav_menus(
    array(
      'header-menu' => __('Main Header Navigation'))
  );
}


// Sidebar Registration.
if (function_exists('register_sidebar')) {
  register_sidebar(array( 'name' => 'Banner Widgets', 'id' => 'banner-widgets'));
}


// Ignore certain items.
// Set via "Hide from Navigation" under Page Layout when editing a page.
function ids_excluded_from_menu() {
    $post_ids = array();
    $args = array(
        'post_type' => 'page',
        'meta_key' => 'hide_from_navigation',
        'meta_value' => true,
        'nopaging' => true
    );

    $pages = new WP_Query($args);

    if ($pages){
        $post_ids = wp_list_pluck( $pages->posts, 'ID' ); // comes out as array.
        $post_ids = implode(",", $post_ids); // makes a simple list split by comma 1,2,3
    }

    return $post_ids;
}


/* Menu for the main navigation bar. */
function nav_menu_primary() {
  global $post;

  /* Defined in the theme settings. */
  $menu_name = 'header-menu';

  if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $menu = wp_get_nav_menu_object( $locations[$menu_name] );

    if ($menu) {
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu_list  = '<ul class="navbar-nav">' ."\n";

        if ($menu and is_array($menu_items)) {
            foreach( $menu_items as $menu_item ) {
                $item_link  = $menu_item->url;
                $item_title = $menu_item->title;
                $item_attr  = $menu_item->attr_title;
                $page_id    = get_post_meta($menu_item->ID, '_menu_item_object_id', true);

                $is_current = (($page_id == $post->ID) or (in_array($page_id, get_post_ancestors($post->ID)))) ? ' active' : '';

                $menu_list .= '<li class="nav-item ' . $is_current . '">' ."\n";
                $menu_list .= '<a href="'. $item_link .'" title="' . $item_attr . '" class="nav-link">'. $item_title .'</a>' ."\n";
                $menu_list .= '</li>' ."\n";
            }
        }
        $menu_list .= '</ul>' ."\n";

        echo $menu_list;
    }
  }
}

/* Secondary navigation bars. */
function nav_bar_secondary() {
  global $post;

  /* Get an array of all the parents for the current page. */
  $ancestors          = array_reverse(get_ancestors($post->ID, 'page'));
  $ancestor_oldest    = ($ancestors ? $ancestors[0] : 0);

  /* Secondary navigation is a fixed menu set in the theme settings.
  * This is used by code.blender.org, while blender.org simply lists child pages. */
  $show_secondary_menu = has_nav_menu('secondary-menu');

  /* Add the current page to the array, to get siblings (in case of top pages). */
  array_push($ancestors, $post->ID);

  $excluded_ids = ids_excluded_from_menu();

  $i = 0;

  /* Make a navbar-secondary element per ancestor. */
  foreach($ancestors as $ancestor) {
    $pages = get_pages(
      array(
        'child_of' => $ancestor,
        'parent' => $ancestor,
        'exclude' => $excluded_ids,
        'sort_column' => 'menu_order'
        )
      );

    /* Show parent link if:
     * There is no fixed secondary menu.
     * The current ancestor is the oldest/top-most level. (e.g. only show on 2nd level nav, not 3rd level)
     * There are less than 2 ancestors.
     * The current ancestor is not in the excluded ids list,
     * and if it's the first navigation bar (not tertiary navbar). */
    $show_parent = !$show_secondary_menu and ((($ancestor == $ancestor_oldest) or (count($ancestors) < 2)) and !in_array($ancestor, array($excluded_ids))) and ($i < 1);

    if ($pages or $show_secondary_menu) {

      /* Currently only the code blog has a fixed secondary menu. */
      if($show_secondary_menu) {
      ?>
        <nav class="navbar navbar-secondary" role="navigation">
        <?php
        wp_nav_menu(
          array(
            'theme_location' => 'secondary-menu',
            'container'       => 'div',
            'container_class' => 'container',
            'menu_class'      => 'navbar-nav',
            'menu_id'         => 'navbar-secondary',
            'add_li_class'    => 'nav-item'
          )
        );
        ?>
        </nav>
  <?php } ?>

  <nav class="navbar navbar-secondary" role="navigation">
    <div class="container">
      <ul class="navbar-nav">
        <?php
          /* Dimmed link to the parent page, if any. */
          if ($show_parent): ?>
            <li class="nav-item nav-parent">
              <a class="nav-link" href="<?=get_the_permalink($ancestor)?>">
                <?=get_the_title($ancestor)?>
              </a>
            </li>
            <li class="nav-item nav-separator">
              <i class="i-chevron-right"></i>
            </li>
        <?php endif; ?>

        <?php
          foreach($pages as $page):
            /* Highlight current or parent pages. */
            $is_current = (in_array($page->ID, [$post->ID, wp_get_post_parent_id($post->ID)]) ? ' active' : '');
          ?>
            <li class="nav-item<?=$is_current?>">
              <a class="nav-link" href="<?=get_permalink($page)?>">
                <?=$page->post_title?>
              </a>
            </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </nav>

  <?php
    $i++; /* Count so show_parent only shows if we are not in the third level. */

    } /* if $pages or $show_secondary_menu */
  } /* foreach ancestor. */
}

function navbar_add_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'navbar_add_class_on_li', 1, 3);


// Override Editor Options
add_filter('tiny_mce_before_init', 'override_mce_options');
function override_mce_options($initArray) {
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
}


// Page/Post Thumbnails
add_theme_support('post-thumbnails');

// Custom size for square thumbnails, also cropped from the center.
add_image_size('square', 200, 200, array('center', 'center'));

// 16 by 9. (25% of FHD) For cards like in homepage news, features, training.
add_image_size('thumbnail_card', 480, 270, array('center', 'center'));

// Default is the listing size, cropped from the center
set_post_thumbnail_size(480, 270, array('center', 'center'));

// OpenGraph: Page title.
function get_site_title($description = false){
    $title = '';
    if (!is_front_page()):
        $title .= wp_title('', false,'') . ' &mdash; ';
    endif;

    $title .= get_bloginfo('name');

    if (is_front_page() or $description == true):
        $title .= ' - ' . get_bloginfo('description');
    endif;

    return $title;
}

// OpenGraph: Page/post description.
function get_shareable_description(){
    $description = get_bloginfo('description');

    if (has_excerpt()):
        $description = get_the_excerpt();
    endif;

    return $description;
}

// OpenGraph:  Page/post image.
function get_shareable_image(){
    $image = get_template_directory_uri() . '/assets_shared/images/blender_the_freedom_to_create_02.jpg';
    $custom_image = get_field('social_media_image');

    if ($custom_image):
        $image = $custom_image['url'];
    elseif (has_post_thumbnail()):
        $image = get_the_post_thumbnail_url();
    endif;

    return $image;
}

/**
 * Trim the excerpt length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999);
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}


// Add Excerpts Support to Pages
add_action( 'init', 'pages_excerpt_add' );
function pages_excerpt_add() {
    add_post_type_support( 'page', 'excerpt' );
}


// Advanced Custom Fields Options Page
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Sitewide Settings',
        'menu_title'    => 'blender.org',
        'menu_slug'     => 'custom-settings'
    ));
}


/* Get the mirror list from the Site-wide Settings page (the list is built with ACF) */
function get_mirrors($skip_mirror = null) {
    $mirrors = get_field('mirrors', 'option');
    $mirrors_active = array();

    if (!$mirrors) {
        trigger_error('No mirrors found. Check Sitewide Settings page config.', E_USER_ERROR);
    }

    foreach ($mirrors as $mirror) {
        /* Only add a mirror to hte list if:
         * * Mirror is enabled.
         * * The URL is different than the mirror picked by GeoIp2 ($skip_mirror).
         * * If it's not already on the list.
         */

        if (!$mirror['active']){
            continue;
        }

        if ($skip_mirror && ($mirror['url'] == $skip_mirror['url'])){
            continue;
        }

        if (!in_array($mirror, $mirrors_active)){
            array_push($mirrors_active, $mirror);
        }
    }

    return $mirrors_active;
}

/* Remove mirrors with the same URL
 * Some entries are duplicated to serve multiple continents with the same mirror.
 * e.g. the North American mirror also serves South America. */
function mirrors_remove_duplicate($mirrors) {
    $mirrors_unique = array_values(
        array_reduce($mirrors, function($r, $a){
            if (!isset($r[$a['active'] . $a['url']])) $r[$a['active'] . $a['url']] = $a;
            return $r;
        }, [])
    );

    return $mirrors_unique;
}


/**
 * Select the closest mirror to the user, based on their location.
 *
 * We look up the COUNTRY_CODE, if there's a match with one of the mirrors we add it to the list. If we don't find
 * a match we try REGION_CODE. If multiple mirror exist, select one randomly.
 *
 * @return array of the closest mirror's properties.
 */
function get_mirror($code_country, $code_continent) {
    $mirrors_all = get_mirrors();
    $mirrors = array();
    $mirrors_in_country = array();

    foreach ($mirrors_all as $mirror) {
        // Append mirrors that match the user's country.
        if ($code_country == $mirror['code_country']['value']) {
            array_push($mirrors_in_country, $mirror);
        }
        // Append mirrors that match the continent only if we don't have a match with the user's country.
        elseif (empty($mirrors_in_country) && $code_continent == $mirror['code_continent']) {
            array_push($mirrors, $mirror);
        }
    }

    if (!empty($mirrors_in_country)) {
        // Replace any continent mirrors with the one from our country only
        $mirrors = $mirrors_in_country;
    } elseif (count($mirrors) == 0) {
        // If nothing was matched, select all mirrors available.
        $mirrors = $mirrors_all;
    }

    $rand_index = array_rand($mirrors);
    return $mirrors[$rand_index];
}


/* Redirect for downloads (Blender, Asset Bundles) to use mirrors.
 * Needs to refresh the cache. Save Permalinks, and flush cache with WC3. */
add_action('init', 'blender_download_mirror');
function blender_download_mirror() {
    // Optionally use ^download/Blender\d\.\d+\S*/blender?
    add_rewrite_rule('^download/release/Blender?', 'index.php?pagename=thanks', 'top');
    add_rewrite_rule('^download/demo/([a-z0-9-]+)[/]?', 'index.php?pagename=thanks', 'top');
    add_rewrite_rule('^download/source/?', 'index.php?pagename=thanks', 'top');
}


function updateDownloadCount($build_name, $mirror_url)
{

    /*
    This function requires the following table to exist:
    CREATE TABLE `blender_downloads` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `build_name` varchar(64) DEFAULT NULL,
      `downloads` int(11) DEFAULT NULL,
      `date` date DEFAULT NULL,
      `mirror` varchar(128) DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
    */

    global $wpdb;
    $mirror_hostname = parse_url($mirror_url)['host'];
    $date = date('Y-m-d');
    // Build filter for a combination of build_name, date and mirror
    $result = $wpdb->get_results(
        $wpdb->prepare(
                "
                SELECT * FROM blender_downloads
                WHERE build_name = %s
                AND date = %s
                AND mirror = %s
                ",
                $build_name,
                $date,
                $mirror_hostname
           ));

    // If no combination of build_name, date and mirror is found, insert row
    if(empty($result)) {
        $wpdb->query(
           $wpdb->prepare(
                "
                INSERT INTO blender_downloads
                ( build_name, downloads, date, mirror )
                VALUES ( %s, %d, %s, %s )
                ",
                $build_name,
                1,
                $date,
                $mirror_hostname
           )
        );
    } else {
        // If a combination of build_name, date and mirror is found, update the downloads count by one
        $wpdb->query(
           $wpdb->prepare(
                "
                UPDATE blender_downloads
                SET downloads = downloads + 1
                WHERE build_name = %s
                AND date = %s
                AND mirror = %s
                ",
                $build_name,
                $date,
                $mirror_hostname
            )
        );
    }
}


// General Tools
function slugify($string){
    $slugified = str_replace("/", "-", $string);
    $slugified = trim(preg_replace("/\\s+/", " ", $slugified));

    // Replace spaces with dashes.
    $slugified = strtolower(str_replace(" ", '-', $slugified));

    // Remove special characters.
    $slugified = preg_replace("/[^A-Za-z0-9\-]/", "", $slugified);

    // Remove recurring dashes (e.g. when "2000 - 2010" converts to "2000---2010")
    $slugified = preg_replace("/([-])\\1+/", "$1", $slugified);

    return $slugified;
};

/* Remove www, protocol, and slashes from URLs. */
function pretty_url($url){
    $url_pretty = preg_replace("#^[^:/.]*[:/]+#i", "", preg_replace("{/$}", "", urldecode($url)));
    $url_pretty = preg_replace('/^www\./', '', $url_pretty);

    return $url_pretty;
}

// Use slug as class on body for page-wide styling
function get_class_body(){
    global $post;

    $slug = 'generic';

    if ($post):
        $slug = $post->post_name;
        $header_large = (get_field('header_size') ? ' has-header' : '');
    endif;

    $class_body = 'ps-' . $slug . $header_large;

    return $class_body;
}


// Find out if we are in the Code Blog based on the active theme (see style.css)
function is_code_blog(){
    return get_stylesheet() == 'bthree-code';
}


/* Navigation: add class to links for styling. */
function add_nav_menu_link_class( $atts, $item, $args ) {
    $atts['class'] = 'nav-link';
    return $atts;
}

add_filter( 'nav_menu_link_attributes', 'add_nav_menu_link_class', 10, 3);


// Training & Courses.
function acf_load_training_trainer_choices( $field ) {
    global $post;

    if(!is_object($post))
        return;

    // Get the parent of courses (must use the Training Homepage template).
    $parent_id = wp_get_post_parent_id($post->ID);
    $field['choices'] = array();

    if( have_rows('trainers', $parent_id) ) {
            while( have_rows('trainers', $parent_id) ) {

                    the_row();

                    $label = get_sub_field('trainer_name');
                    $value = get_row_index();

                    $field['choices'][$value] = $label; // append to choices
            }
    }
    return $field;
}

add_filter('acf/load_field/name=course_trainer', 'acf_load_training_trainer_choices');


function acf_load_training_status_choices( $field ) {
    global $post;

    if(!is_object($post))
        return;

    // Get the parent of courses (must use the Training Homepage template).
    $parent_id = wp_get_post_parent_id($post->ID);
    $field['choices'] = array();

    if( have_rows('statuses', $parent_id) ) {
            while( have_rows('statuses', $parent_id) ) {

                    the_row();

                    $label = get_sub_field('status_name');
                    $value = get_row_index();

                    $field['choices'][$value] = $label; // append to choices
            }
    }
    return $field;
}

add_filter('acf/load_field/name=course_status', 'acf_load_training_status_choices');


function acf_load_training_price_choices( $field ) {
    global $post;

    if(!is_object($post))
        return;

    // Get the parent of courses (must use the Training Homepage template).
    $parent_id = wp_get_post_parent_id($post->ID);
    $field['choices'] = array();

    if( have_rows('prices', $parent_id) ) {
        while( have_rows('prices', $parent_id) ) {

            the_row();

            $label = get_sub_field('price_amount');
            $value = get_row_index();

            $field['choices'][$value] = $label; // append to choices
        }
    }
    return $field;
}

add_filter('acf/load_field/name=course_price_amount', 'acf_load_training_price_choices');


// Expand the list of allowed MIME types for upload.
function custom_mime_types($mimes) {

    // Use 'application/x-gzip' for compressed .blend files, or 'x-blender' for uncompressed.
    $mimes['blend'] = 'application/x-gzip';

    return $mimes;
}

add_filter( 'upload_mimes', 'custom_mime_types' );


// UI Components.
function show_card_label($label){
    if ($label):
        echo '<em class="d-block text-info">' . $label . '</em>';
    endif;
}


/* Filters for the_content. */

/* Parse headings and convert them into an anchor. */
function the_content_filter_headings( $content ) {

    /* Only affect posts, not pages. */
    if ('post' !== get_post_type()) {
        return $content;
    }

    $dom = new DOMDocument();
    libxml_use_internal_errors(true);
    $dom->loadHTML('<?xml encoding="utf-8"?>' . $content);
    $dom->encoding='UTF-8';

    /* Parse these heading sizes. */
    $headings_to_link = ['h1', 'h2', 'h3'];

    foreach($headings_to_link as $heading_size) {
        foreach($dom->getElementsByTagName($heading_size) as $heading) {
            // Create link element.
            $link = $dom->createElement("a");

            // Get heading id.
            $heading_id = $heading->getAttribute('id');

            if (!$heading_id) {
                $heading_id = slugify($heading->nodeValue);
                $heading->setAttribute('id', $heading_id);
            }

            $link->setAttribute('href', "#" . $heading_id);
            $link->setAttribute('class', 'is-heading-anchor');
            $parent = $heading->parentNode;
            $parent->insertBefore($link, $heading);
            $link->appendChild($heading);
        }
    }

    return $dom->saveHtml();
};

add_filter( 'the_content', 'the_content_filter_headings', 10, 1 );


// Remove "Website" field from comments form.
function comments_remove_website_field($fields) {
   unset($fields['url']);
   return $fields;
}
add_filter('comment_form_default_fields', 'comments_remove_website_field');


/* Whether to show an image or video tags.
 * Used in Cards block. */
function mediaThumbnail($filename, $name) {
	$extension = pathinfo($filename)['extension'];
	$video_ext = ['mp4', 'mov', 'webm'];

	if (in_array($extension, $video_ext)) {
		echo '
			<video loop autoplay muted preload="metadata">
				<source src="' . $filename . '" type="video/' . $extension . '">
			</video>'
		;
	} else {
		echo '<img src="' . $filename . '" alt="' . $name . '"/>';
	}
}


// Shortcodes.
function b3_shortcode_blender_version() {

    $label = get_field('blender_version', 'option');
    return $label;
}

add_shortcode('blender_version', 'b3_shortcode_blender_version');

function b3_shortcode_mirrors_list() {

    $mirrors = get_mirrors();
    $mirrors_unique = mirrors_remove_duplicate($mirrors);

    $return = '<ul>';
    foreach ($mirrors_unique as $mirror){
        $return .= '<li>';
        $return .= '<a href="' . $mirror['url'] . '" target="_blank">';
        $return .= $mirror['code_country']['label'] . ' - ' . $mirror['name'];
        $return .= '</a>';
        $return .= '</li>';
    }
    $return .= '</ul>';

    return $return;
}

add_shortcode('mirrors', 'b3_shortcode_mirrors_list');


// Custom Blocks Category
function b3_block_category( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'b3',
                'title' => __( 'Blender.org Blocks', 'b3' ),
            ),
        )
    );
}
add_filter( 'block_categories_all', 'b3_block_category', 10, 2);


// Custom ACF Blocks
function register_acf_block_types() {

    acf_register_block_type(array(
        'name'              => 'accordion',
        'title'             => __('Accordion (deprecated)'),
        'description'       => __('Use the Details block instead'),
        'render_template'   => '/blocks/accordion.php',
        'category'          => 'b3',
        'icon'              => 'editor-insertmore',
        'keywords'          => array( 'accordion', ),
    ));

    acf_register_block_type(array(
        'name'              => 'cards',
        'title'             => __('Cards'),
        'description'       => __('A list of card elements.'),
        'render_template'   => '/blocks/cards.php',
        'category'          => 'b3',
        'icon'              => 'grid-view',
        'keywords'          => array( 'cards', ),
    ));

    acf_register_block_type(array(
        'name'              => 'chart-bars',
        'title'             => __('Chart Bars'),
        'description'       => __('A list of bars to use as stats.'),
        'render_template'   => '/blocks/chart-bars.php',
        'category'          => 'b3',
        'icon'              => 'chart-bar',
        'keywords'          => array( 'chart-bars', 'stats', 'chart', 'graph'),
    ));

    acf_register_block_type(array(
        'name'              => 'compare-media',
        'title'             => __('Compare Media'),
        'description'       => __('Slide between two media items to compare them.'),
        'render_template'   => '/blocks/compare-media.php',
        'category'          => 'b3',
        'icon'              => 'image-flip-horizontal',
        'keywords'          => array( 'compare', ),
    ));

    acf_register_block_type(array(
        'name'              => 'pages-list',
        'title'             => __('List of Pages'),
        'description'       => __('List pages as cards.'),
        'render_template'   => '/blocks/pages-list.php',
        'category'          => 'b3',
        'icon'              => 'editor-paragraph',
        'keywords'          => array( 'pages', 'list', 'card'),
    ));

    acf_register_block_type(array(
        'name'              => 'paragraph-plus',
        'title'             => __('Paragraph+'),
        'description'       => __('Multiple images or videos organized in paragraph-plus.'),
        'render_template'   => '/blocks/paragraph-plus.php',
        'category'          => 'b3',
        'id'                => '',
        'icon'              => 'editor-paragraph',
        'keywords'          => array( 'paragraph-plus', ),
    ));

    acf_register_block_type(array(
        'name'              => 'social-cards',
        'title'             => __('Social Cards'),
        'description'       => __('A list of cards pointing to social media.'),
        'render_template'   => '/blocks/social-cards.php',
        'category'          => 'b3',
        'icon'              => 'grid-view',
        'keywords'          => array( 'cards', 'social', 'twitter', 'youtube'),
    ));

    acf_register_block_type(array(
        'name'              => 'tabs',
        'title'             => __('Tabs'),
        'description'       => __('Multiple images or videos organized in tabs.'),
        'render_template'   => '/blocks/tabs.php',
        'category'          => 'b3',
        'icon'              => 'images-alt2',
        'keywords'          => array( 'tab', 'tabs', ),
    ));

    acf_register_block_type(array(
        'name'              => 'video-scroll',
        'title'             => __('Video Scroll'),
        'description'       => __('A video that plays as you scroll.'),
        'render_template'   => '/blocks/video-scroll.php',
        'category'          => 'b3',
        'icon'              => 'playlist-video',
        'keywords'          => array( 'video-scroll', 'video', 'scroll' ),
    ));

    acf_register_block_type(array(
        'name'              => 'word-cloud',
        'title'             => __('Word Cloud'),
        'description'       => __('A centered list of words or links.'),
        'render_template'   => '/blocks/word-cloud.php',
        'category'          => 'b3',
        'icon'              => 'admin-links',
        'keywords'          => array( 'word-cloud', 'links', 'cloud' ),
    ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
        add_action('acf/init', 'register_acf_block_types');
}

// Custom styling for editor.
function b3_editor_css(){
    add_theme_support( 'editor-styles' );
    add_editor_style( 'style-editor.css' );
}

add_action( 'after_setup_theme', 'b3_editor_css' );

// Javascript to customize blocks.
function b3_enqueue() {
        wp_enqueue_script(
                'b3-script',
                get_template_directory_uri() . '/assets/js/bthree.js',
                array( 'wp-blocks', 'wp-dom', 'wp-dom-ready', 'wp-edit-post'),
                time(),
                true
        );
}
add_action( 'enqueue_block_editor_assets', 'b3_enqueue' );

/* Admin: Add column with Template name to the list of pages.
 * Source: https://www.isitwp.com/custom-column-with-currently-active-page-template/ */
function page_column_views( $defaults )
{
    $defaults['page-layout'] = __('Template');
    return $defaults;
}
function page_custom_column_views( $column_name, $id )
{
    if ( $column_name === 'page-layout' ) {
        $set_template = get_post_meta( get_the_ID(), '_wp_page_template', true );
        if ( $set_template == 'default' ) {
            echo 'Default';
        }
        $templates = get_page_templates();
        ksort( $templates );
        foreach ( array_keys( $templates ) as $template ) :
            if ( $set_template == $templates[$template] ) echo $template;
        endforeach;
    }
}
add_filter( 'manage_pages_columns', 'page_column_views' );
add_action( 'manage_pages_custom_column', 'page_custom_column_views', 5, 2 );


/* Remove the 'Protected:' part from post title. */
function remove_protected_text() {
    return __('%s');
}
add_filter( 'protected_title_format', 'remove_protected_text' );


/* Hide password protected posts from navigation. */
function remove_password_post_links_adjacent($where) {
    return $where ." AND post_password = '' ";
}

add_filter('get_previous_post_where', 'remove_password_post_links_adjacent');
add_filter('get_next_post_where', 'remove_password_post_links_adjacent');


/* Hide unpublished and password protected posts from listing.
 * Only show to people that can edit posts (editors and admins). */
function list_posts_filter( $where = '' ) {
   if (!is_single() && !current_user_can('edit_posts') && !is_admin()) {
        $where .= " AND post_password = ''";
    }

    return $where;
}
add_filter( 'posts_where', 'list_posts_filter' );


/* Format bytes (e.g. for showing builds size) */
function format_bytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'KB', 'MB', 'GB', 'TB');

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}

?>
