<?php
/*
Template Name: Download: Main
*/

// IDs of pages for creating links with get_permalink()
$ids = [
	'features' => 57630
	];

// Global settings coming from blender.org options (see admin sidebar)
$blender_version       = get_field('blender_version', 'option');
$blender_version_base  = explode('.', $blender_version);
$blender_version_base  = $blender_version_base[0] . '.' . $blender_version_base[1]; // Drop the fix release for display

$release_date          = get_field('blender_release_date', 'option');

// Platforms and mirrors
$builds_folder         = get_field('builds_folder');
$platforms             = get_field('platforms');

$splash                = get_field('splash');
$release_notes_summary = get_field('release_notes_summary');
$release_notes_url     = get_field('release_notes_url');

$filepath_md5          = get_field('filepath_md5');
$filepath_sha256       = get_field('filepath_sha256');

/* Small piece of text/html to include under the Download button.
To be used for small announcements like an Alpha/Beta. */
$announcement          = get_field('announcement_content');

/* Header layout and style. */
$header_title          = get_field('header_title');
$header_subtitle       = get_field('header_subtitle');
$header_credits        = get_field('header_background_credits');
$header_image_offset   = get_field('header_image_offset');
$header_image_src      = get_the_post_thumbnail_url($post->ID, 'full');
$background_overlay    = get_field('background_overlay');

$header_align   = get_field('header_align');
$header_align_x = isset($header_align) ? $header_align['header_align_x'] : 'header-align-x-center';
$header_align_y = isset($header_align) ? $header_align['header_align_y'] : 'header-align-y-center';

get_header();

$analytics_event_name = 'Downloads+Blender';
?>

<?php
/* Fallback to showing Windows download links in case JavaScript is disabled. */ ?>
<noscript>
	<style>
		.dl-os-windows { display: block !important;}
		.dl-header-other-list{
			display: block !important;
			left: unset !important;
			opacity: 1 !important;
			position: initial !important;
			transform: unset !important;
			visibility: visible !important;
		}
	</style>
</noscript>

<section class="hero download <?=$header_align_x?> <?=$header_align_y?>">
	<div class="container">
		<div class="hero-content">
			<h1>
				<?=($header_title ? $header_title : wp_title(''))?>
			</h1>

			<?php if ($header_subtitle): ?>
				<div class="hero-subtitle"><?=$header_subtitle?></div>
			<?php endif; ?>

			<?php
				echo '<div class="dl-buttons-container">';
				/* For each platform, display a large Download button and build details. */
				foreach($platforms as $platform):
					$main_platform_path = $platform['builds'][0]['filepath'];
					$main_platform_title = $platform['builds'][0]['title'];

					$analytics_event_os    = $platform['name'];
					$analytics_event_build = $analytics_event_os . ($main_platform_title ? '+' . str_replace(' ', '+', $main_platform_title) : '');

					if ($platform['builds'][0]['size']) {
						$main_platform_size = number_format($platform['builds'][0]['size'] / 1024 / 1024);
					};

					$url = get_permalink(get_the_ID()) . $builds_folder . '/' . $main_platform_path . '/';

					echo '
						<div id="' . $platform['slug'] . '" class="dl-header-cta dl-os-' . $platform['slug'] . '">';

					/* Big download button. */
					echo '
							<a
								href="' . $url . '"
								class="btn btn-accent dl-header-cta-button plausible-event-name=' . $analytics_event_name . ' plausible-event-os=' . $analytics_event_os . ' plausible-event-build=' . $analytics_event_build . '"
								title="Download Blender for ' . $platform['name'] . ' ' . $main_platform_title . '">
								<i class="i-' . $platform['slug'] . '"></i>
								Download Blender ' . $blender_version . '
							</a>';

					/* Build details. */
					echo '
							<ul class="dl-build-details">
								<li class="os ' . $platform['slug'] . '">'
									. '<strong>'. $platform['name'] . '</strong>' .
								'</li>' .
								($main_platform_title ? '<li>' . $main_platform_title . '</li>' : '') .
								($main_platform_size ? '<li title="Tiny isn\'t?">'  . $main_platform_size . 'MB</li>' : '') .
								'<li class="popup-toggle js-toggle-menu" data-toggle-menu="menu-info-' . $platform['slug'] . '"><i class="i-info"></i></li>
							</ul>';

					/* Pop-up shown when clicking on the info icon.
					* Displaying info such as build requirements, md5, and sha256. */
					echo '<div class="dl-build-details-popup dl-togglable" id="menu-info-' . $platform['slug'] . '">';

						echo '<div class="platform-warning alert alert-warning"></div>';

						if ($platform['info']):
							echo  $platform['info'];
						endif;

						echo '<small>Released on ' . $release_date . ' · </small>';

						// Checksums.
						if ($filepath_md5):
							echo '<small class="checksum"><a href="' . $filepath_md5 . '">md5</a></small>';
						endif;

						if ($filepath_sha256):
							echo '<small class="checksum"><a href="' . $filepath_sha256 . '">sha256</a></small>';
						endif;
					echo '</div>';
				echo '</div>';
			endforeach; // $platforms as $platform

			echo '</div>'; // .dl-buttons-container

			/* Dropdown button to show other available versions. */
			echo '<div class="dl-header-other js-toggle-menu" data-toggle-menu="menu-other-platforms">
							<span class="ot windows">Windows,</span>
							<span class="ot macos">macOS,</span>
							<span class="ot linux">Linux,</span>
							and other versions <i class="i-chevron-down"></i>
						</div>';

			echo '<ul class="dl-header-other-list dl-togglable" id="menu-other-platforms">';

			/* List of all platforms, and builds for each platform. */
			foreach($platforms as $i => $other_platform):
					foreach ($other_platform['builds'] as $build):

						$is_external_url = $build['is_external_url'];
						$url = get_permalink(get_the_ID()) . $builds_folder . '/' . $build['filepath'] . '/';
						$other_platform_title = $build['title'];

						$analytics_event_os    = $other_platform['name'];
						$analytics_event_build = $analytics_event_os . ($other_platform_title ? '+' . str_replace(' ', '+', $other_platform_title) : '');

						if ($is_external_url){
							$url = $build['url'];
						}

							$list_item  = '<li ';
							$list_item .= 'class="os ' . $other_platform['slug'] . '">';
							$list_item .= '<a ';
							$list_item .= 'class="plausible-event-name=' . $analytics_event_name . ' plausible-event-os=' . $analytics_event_os . ' plausible-event-build=' . $analytics_event_build . '"';
							$list_item .= 'href="' . $url . '"';
							$list_item .= 'title="Download Blender for ' . $other_platform['name'] . ' ' . $build['title'] . '"';
							if ($is_external_url){
								$list_item .= 'target="_blank"'; // open external links in a new tab.
							}
							$list_item .= '>'; // close <a> tag.
							$list_item .= '<span class="name">' . $other_platform['name']  . '</span>';

							if ( $build['title'] ){
								$list_item .= '<span class="build">'  . $build['title'] . '</span>';
							}

							if ( $build['size'] ){
								$build_size = format_bytes($build['size'], 1);
								$list_item .= '<span class="size">'  . $build_size . '</span>';
							}

							$list_item .= '</a>';
							$list_item .= '</li>';

							echo $list_item;

						endforeach; // $platform['builds'] as $build

						if ($i !== array_key_last($platforms)) {
							echo '<li class="separator"></li>';
					}
				endforeach; // $platforms as $other_platform

				echo '</ul>'; // End of other platforms.
				?>

				<?php if ($announcement): ?>
					<div class="dl-announcement">
						<?=($announcement)?>
					</div>
				<?php endif; ?>
		</div>
	</div>

	<?php if ($header_image_src): ?>
	<div class="hero-background-faded">
		<div class="hero-background-faded-image" style="top: <?=$header_image_offset?>">
			<img src="<?=$header_image_src?>" alt=""/>
		</div>
	</div>
	<?php endif;?>

	<?php if ($background_overlay): ?>
	<div class="hero-overlay" style="background-color: <?=$background_overlay?>"></div>
	<?php endif; ?>

	<div class="hero-overlay hero-overlay-bottom"></div>
	<div class="hero-overlay hero-overlay-top"></div>

	<?php if ($header_credits): ?>
	<div class="hero-credits"><?=$header_credits?></div>
	<?php endif;?>
</section>

<div class="dl-features">
	<a href="<?=$release_notes_url?>">
		<div class="dl-background-image" style="background-image: url(<?=$splash['url']?>);"></div>
	</a>
	<div class="dl-overlay-text"></div>
	<div class="dl-overlay-text"></div>
	<div class="dl-overlay"></div>
	<div class="dl-overlay bottom"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 ml-auto">
				<div class="dl-features-text">
					<a href="<?=$release_notes_url?>">
						<h1>What's New</h1>
					</a>

					<?=$release_notes_summary?>

					<div>
						<a class="btn btn-md px-5" href="<?=$release_notes_url?>">
							<span>New in Blender <?=$blender_version_base?></span>
						</a>
					</div>

					<div class="dl-features-moar">
						<a href="<?=get_permalink($ids['features'])?>">See all Blender features <i class="i-chevron-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="dl-bleeding">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<a href="https://builder.blender.org/download/daily/">
					<h1>Go Experimental</h2>
				</a>

				<p>Blender evolves every day. Experimental builds have the latest features
				and while there might be cool bug fixes too, they are unstable
				and can mess up your files.</p>

				<p>It is <strong>not recommended</strong> to use these on production environments.</p>

				<a href="https://builder.blender.org/download/daily/" class="btn btn-md px-5">
					<span>Download Blender Experimental</span>
				</a>
			</div>
			<div class="col-md-6">
				<div class="dl-bleeding-icon">
					<a href="https://builder.blender.org/download/daily/">
						<svg width="110px" height="110px" viewBox="-20 -10 92 92"><g><g><g><path d="M22,55c-0.259,0-0.515-0.1-0.708-0.293l-12-12c-0.327-0.326-0.387-0.834-0.147-1.228c0.063-0.103,6.418-10.459,16.129-20.669C38.226,7.196,51.271,0,63,0c0.552,0,1,0.448,1,1c0,11.729-7.196,24.774-20.811,37.725C32.98,48.436,22.624,54.791,22.521,54.854C22.359,54.952,22.179,55,22,55z M11.272,41.858l10.87,10.87c5.183-3.326,38.931-25.923,39.839-50.709C37.195,2.927,14.599,36.675,11.272,41.858z"/></g><g><path d="M17,56c-0.256,0-0.512-0.098-0.707-0.293l-8-8c-0.391-0.391-0.391-1.024,0-1.414l3-3l1.414,1.414L10.414,47L17,53.586l2.293-2.293l1.414,1.414l-3,3C17.512,55.902,17.256,56,17,56z"/></g><g><path d="M1,64c-0.552,0-1-0.448-1-1c0-0.303,0.071-7.486,6.293-13.707l1.414,1.414c-4.251,4.251-5.336,9.078-5.613,11.199c2.119-0.276,6.947-1.361,11.199-5.613l1.414,1.414C8.486,63.928,1.303,64,1,64z"/></g><g><path d="M46,23c-2.757,0-5-2.243-5-5s2.243-5,5-5s5,2.243,5,5S48.757,23,46,23z M46,15c-1.654,0-3,1.346-3,3s1.346,3,3,3s3-1.346,3-3S47.654,15,46,15z"/></g><g><path d="M37,32c-2.757,0-5-2.243-5-5s2.243-5,5-5s5,2.243,5,5S39.757,32,37,32z M37,24c-1.654,0-3,1.346-3,3s1.346,3,3,3s3-1.346,3-3S38.654,24,37,24z"/></g><g><path d="M28,41c-2.757,0-5-2.243-5-5s2.243-5,5-5s5,2.243,5,5S30.757,41,28,41z M28,33c-1.654,0-3,1.346-3,3s1.346,3,3,3s3-1.346,3-3S29.654,33,28,33z"/></g><g><path d="M37,64H27c-0.552,0-1-0.448-1-1v-8h2v7h8.352l16.735-37.408l1.825,0.817l-17,38C37.752,63.768,37.395,64,37,64z"/></g><g><path d="M9,38H1c-0.552,0-1-0.448-1-1V27c0-0.394,0.232-0.752,0.592-0.913l38-17l0.816,1.826L2,27.648V36h7V38z"/></g><g><path d="M61,12c-4.962,0-9-4.037-9-9h2c0,3.86,3.14,7,7,7V12z"/></g></g></g></svg>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php function scripts() { ?>
<script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/get_os.js"></script>
<script>

	/* Populate a warning alert for specific platforms. */
	function showPlatformWarning(content){
		$('.dl-build-details, .dl-build-details-popup').addClass('has-alert alert-warning');
		$('.popup-toggle').html('<i class="i-alert-triangle"></i>');

		$('.platform-warning')
			.addClass('show')
			.html(content);
	}

	$(document).ready(function(	){

		$('.js-toggle-menu').on('click', function(e){
				e.stopPropagation();

				let toggle = $(this).data('toggle-menu');
				let menu = document.getElementById(toggle);

				$(this).toggleClass('active');
				$(menu).toggleClass('active');
		});

		/* Hide all Download buttons. */
		$('[class*="dl-os-"]').hide().removeClass('active');

		/* Display only one group of Download buttons based on OS, fallback to Windows. */
		let downloadButtons = document.getElementsByClassName('dl-os-windows');

		/* Get the current operating system. See get_os.js */
		let os = getOS();

		/* Windows. */
		if (os == 'windows') {
			showPlatformWarning('This Windows version might not be supported.');
		}
		else if (os == 'windows-arm') {
			showPlatformWarning('Blender is not available for Windows ARM architecture yet.');
		}
		/* Linux. */
		else if (os.startsWith('linux') || os.startsWith('freebsd')) {

			/* Set the Download button platform to Linux. */
			downloadButtons = document.getElementsByClassName('dl-os-linux');

			if (os == 'freebsd') {
				showPlatformWarning('There are no official builds for FreeBSD yet.');
			}
		}
		/* macOS. */
		else if (os.startsWith('macos') || os.startsWith('ios')) {

			/* Set the Download button platform to macOS. */
			downloadButtons = document.getElementsByClassName('dl-os-macos');

			if (os == 'macos-apple-silicon') {
				downloadButtons = document.getElementsByClassName('dl-os-macos-apple-silicon');

				/* Safari does not have a reliable way to detect if the system is Intel or Apple Silicon,
				 * Show Download buttons for both systems for the time being until navigator.gpu is supported. */
				let is_safari = navigator.userAgent.indexOf('Safari') > -1 && navigator.userAgent.indexOf('Chrome') <= -1;

				if (is_safari) {
					downloadButtons = document.querySelectorAll('[class^="dl-header-cta dl-os-macos"]');
				}
			} else if (os == 'macos-32' || os == 'macos-PPC') {
				showPlatformWarning('This macOS version might not be supported.');
			}
			else if (os == 'ios') {
				showPlatformWarning('Blender is not available for iOS yet.');
			}
		}

		/* Show the Download button for the current OS. */
		for (var i = 0; i < downloadButtons.length; i++) {
			downloadButtons[i].style.display = 'block';
			downloadButtons[i].classList.add('active');
		}

		/* Style the other platforms button, so we can highlight alternative builds on the same platform. */
		$('#menu-other-platforms').addClass('dl-other-list-os-' + os);
		$('.dl-header-other').addClass('current-os-' + os);

		/* Click anywhere on the page to hide these popups. */
		$(document).click(function () {
			$('.dl-togglable, .js-toggle-menu').removeClass('active');
		});
	});
</script>

<?php }; ?>

<?php add_action( 'wp_footer', 'scripts' ); ?>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
