<?php
	$header_size                = get_field('header_size');
	$header_image_src           = get_the_post_thumbnail_url($post->ID, 'full');

	if (!$header_image_src) {
		// Fallback background if no featured image is set.
		if (is_code_blog()){
			$header_image_src = 'https://www.blender.org/wp-content/themes/bthree/assets/images/background-code.jpg';
		} else {
			$header_image_src = 'https://www.blender.org/wp-content/themes/bthree/assets/images/placeholder_background_small.jpg';
		}
	}

	$header_image_offset        = get_field('header_image_offset');
	$header_credits             = get_field('header_background_credits');

	$header_title               = get_field('header_title');
	$header_subtitle            = get_field('header_subtitle');

	$header_title_size          = get_field('header_title_size');

	/* Calls to Action. */
	$header_cta_primary_label   = get_field('header_cta_primary_label');
	$header_cta_primary_url     = get_field('header_cta_primary_url');
	$header_cta_secondary_label = get_field('header_cta_secondary_label');
	$header_cta_secondary_url   = get_field('header_cta_secondary_url');

	/* Alignment. */
	$header_align   = get_field('header_align');
	$header_align_x = isset($header_align) ? $header_align['header_align_x'] : 'header-align-x-left';
	$header_align_y = isset($header_align) ? $header_align['header_align_y'] : 'header-align-y-center';

	$container_overlap          = get_field('is_container_overlap');
	$is_container_overlap       = (isset($container_overlap) and $container_overlap) ? 'is-container-overlap' : '';

	/* Background Overlay. */
	$background_overlay         = get_field('background_overlay');

	/* Background Video. */
	$header_background_video    = (get_field('background_type') == 'video') ? true : false;
	$background_video_id        = get_field('background_video_id');
	$background_video_start     = get_field('background_video_start');
	$background_video_end       = get_field('background_video_end');

	$hide_navigation            = get_field('navigation_hide');

	/*
	 * - If the page/post has_post_thumbnail, set $header_image_src as background-image url
	 * - If the header is not 'large', set $header_image_offset as background-position-y
	 */
?>

<?php if ($header_size == 'large'): ?>
<div class="hero header-size-<?=$header_size?> <?=$header_align_x?> <?=$header_align_y?> <?=$is_container_overlap?>">
	<div class="container">
		<div class="hero-content">
			<h1 class="<?=(($header_title_size) ? $header_title_size : '')?>">
				<?=($header_title ? do_shortcode($header_title) : wp_title(''))?>
			</h1>

			<?php if ($header_subtitle): ?>
				<div class="hero-subtitle"><?=do_shortcode($header_subtitle)?></div>
			<?php endif; ?>

			<?php if ($header_cta_primary_label or $header_cta_secondary_label): ?>
				<div class="hero-cta-container">
					<div class="btn-row">
						<?php if ($header_cta_primary_label): ?>
							<a href="<?=$header_cta_primary_url?>" class="btn btn-accent">
								<span><?=do_shortcode($header_cta_primary_label)?></span>
							</a>
						<?php endif; ?>
						<?php if ($header_cta_secondary_label): ?>
							<a href="<?=$header_cta_secondary_url?>" class="btn btn-link">
								<span><?=do_shortcode($header_cta_secondary_label)?></span>
							</a>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<?php if ($header_image_src): ?>
	<div class="hero-background" style="background-image: url(<?=$header_image_src?>); background-position-y: <?=$header_image_offset?>"></div>
	<?php endif;?>

	<?php if ($header_background_video): ?>
		<div id="hero-overlay-video" class="hero-overlay-video" style="background-image: url(<?=$header_image_src?>); background-position-y: <?=$header_image_offset?>">
			<div id="bgvideo"></div>
		</div>
	<?php endif; ?>

	<?php if ($background_overlay): ?>
	<div class="hero-overlay" style="background-color: <?=$background_overlay?>"></div>
	<?php endif; ?>

	<?php if ($header_credits): ?>
	<div class="hero-credits"><?=$header_credits?></div>
	<?php endif;?>
</div>

<?php elseif ($header_size == 'title'): ?>

<div class="page-header <?=$header_align_x?>">
	<h1 class="<?=(($header_title_size) ? $header_title_size : '')?>">
		<?=($header_title ? do_shortcode($header_title) : wp_title(''))?>
	</h1>
</div>

<?php endif; ?>


<?php if (($header_size == 'large') and ($header_background_video)): ?>
<script type="text/javascript">
	/* YT Video Background. Modified from https://codepen.io/ccrch/pen/GgPLVW */
	var videoId = "<?=$background_video_id?>";
	var tag = document.createElement('script');
			tag.src = 'https://www.youtube.com/player_api';
	var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	var bgvideo,
			playerDefaults = {autoplay: 0, autohide: 1, modestbranding: 0, rel: 0, loop: true, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 0, iv_load_policy: 3};
	var vid = {'videoId': videoId, 'startSeconds': <?=($background_video_start ? $background_video_start : 0)?>, 'endSeconds': <?=$background_video_end?>, 'suggestedQuality': 'hd720'};

	function onYouTubePlayerAPIReady(){
	  bgvideo = new YT.Player('bgvideo', {events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange, 'onError': onPlayerError}, playerVars: playerDefaults});
	}

	function onPlayerReady(){
	  bgvideo.loadVideoById(vid);
	  bgvideo.mute();
	}

	function onPlayerStateChange(e) {
	  if(e.data === YT.PlayerState.ENDED){
	  	bgvideo.seekTo(vid.startSeconds);
	  }
	}

	function onPlayerError(){
		let video = document.getElementById('featured-overlay-video');
		video.classList += ' hidden';
	}
</script>
<?php endif; ?>
