<?php
/*
Template Name: Features: Main
*/

$features_title        = get_field('features_title');
$features_index        = get_field('features_index');

// TODO(Pablo): Share with Features Category.
$dev_status = [
	"new" => array(
		"title" => 'NEW!',
		"desc" => 'This is a fresh new feature!'
	),
	"upcoming" => array(
		"title" => 'UPCOMING',
		"desc" => 'This feature will be part of the next release!'
	),
	"deprecated" => array(
		"title" => 'DEPRECATED',
		"desc" => "This feature won't part of the next release"
	)
];

?>

<?php get_header(); ?>
<?php get_header('static'); ?>

<div class="container">

	<?php while ( have_posts() ) : the_post(); ?>
		<?php if (!empty(get_the_content())): ?>
			<?=the_content()?>
		<?php endif; ?>
	<?php endwhile; ?>

	<?php /* List of features categories. Cards style. */ ?>
	<div class="features-cards">
		<div class="cards-list">
			<?php while( have_rows('categories') ): the_row(); ?>
				<?php $category_pages = get_sub_field('category_page');

				if($category_pages): ?>

					<?php foreach($category_pages as $post): ?>
						<?php setup_postdata($post); ?>

						<?php $category_dev_status = get_field('category_dev_status'); ?>

							<div class="cards-list-item-outer">
								<div class="cards-list-item-inner">
									<?php if (has_post_thumbnail()): ?>
										<a href="<?php the_permalink(); ?>" class="cards-list-item-thumbnail">
											<?php the_post_thumbnail('featured-list', ['class' => 'img-fluid', 'title' => get_the_title()]); ?>
										</a>
									<?php endif; ?>

									<a class="cards-list-item-title" href="<?php the_permalink(); ?>">
										<?php the_title() ?>

										<?php /* Label with development status of the feature. */ ?>
										<?php if ($category_dev_status && ($category_dev_status != 'implemented')): ?>
											<span class="cards-list-item-label" title="<?=$dev_status[$category_dev_status]['desc']?>">
												<?=$dev_status[$category_dev_status]['title']?>
											</span>
										<?php endif; ?>
									</a>
									<a class="cards-list-item-excerpt" href="<?php the_permalink(); ?>"><?php the_excerpt();?></a>
									<div class="cards-list-item-more">
										<a class="more-item url" href="<?php the_permalink(); ?>">READ MORE  <i class="i-chevron-right"></i></a>
									</div>
								</div>
							</div>

						<?php break; ?>
						<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
	</div>

	<?php /* Index of all the features, subcategories and its details. */ ?>
	<?php if ($features_index){ ?>
	<hr/>

	<div class="features-index">

	<h3>All in One Place</h3>

	<?php while(have_rows('categories')): the_row();

		$category_pages = get_sub_field('category_page'); ?>

		<div class="category-row">
			<?php if($category_pages){ ?>

				<?php foreach($category_pages as $post): ?>

					<?php setup_postdata($post); ?>

					<a class="category-title" href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a>

					<?php $sub_categories = get_field('sub_categories'); ?>

					<?php if ($sub_categories) { ?>

						<?php while( have_rows('sub_categories') ): the_row();
							$subcat_title = get_sub_field('sub_category_title');
							$subcat_slug = get_sub_field('sub_category_slug');

							$subcat_details = get_sub_field('feature_details');
						?>

						<div class="category-sub-row">

							<div class="category-sub-title">
								<a href="<?php the_permalink(); ?>#<?=$subcat_slug;?>">
									<?=$subcat_title;?>
								</a>
							</div>

							<?php if ($subcat_details) { ?>
								<ul class="category-sub-details list-bullets-none">
									<?php	while( have_rows('feature_details') ): the_row();
											$subcat_detail_title = get_sub_field('detail_title');
											$subcat_detail_slug = get_sub_field('detail_slug'); ?>
										<li>
											<a href="<?php the_permalink(); ?>#<?=$subcat_slug;?>/<?=$subcat_detail_slug;?>">
												<?=$subcat_detail_title;?>
											</a>
										</li>
									<?php endwhile; ?>
								</ul>
							<?php }; // endif $subcat_details ?>
						</div> <!-- category-sub-row -->

						<?php endwhile; // $sub_categories ?>

					<?php }; // endif $sub_categories ?>
					<?php break; ?>
				<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		<?php }; // endif $category_pages ?>
		</div>
	<?php endwhile; ?>
	</div>
	<?php }; // endif $features_index ?>
</div>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
