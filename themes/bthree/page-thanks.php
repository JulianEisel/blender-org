<?php
/*
Template Name: Download: Thanks!

This page is displayed when we visit the following endpoints:

- ^download/Blender? (see add_rewrite_rule in functions.php)
- thanks (pushed into history after download)

Works like this:

https://www.blender.org/download/release/Blender2.93/blender-2.93.6-macos-x64.dmg

becomes (depending on geo location). Notice that /download is removed.

https://ftp.nluug.nl/pub/graphics/blender/release/Blender2.93/blender-2.93.6-macos-x64.dmg

TODO: fix use of $mirror_alt as it still uses hardcoded path construction.

*/
define('DONOTCACHEPAGE', true); /* Do not cache. This is a dynamic page due to mirrors and slates. */

$do_bypass_geoip = false;
$do_redirect = (isset($_GET['dev']) and ($_GET['dev'] == True)) ? False : True;

$url = $_SERVER['REQUEST_URI'];
$url = strtok($url, "?"); // Remove everything after question mark (e.g. wp cached links)
$filepath = str_replace('/download/', '', $url);

require_once("vendor/geoip2/geoip2-2.11.0.phar");
use GeoIp2\Database\Reader;

/* Geolocation. */
if ($do_bypass_geoip) {
  $code_country = 'NL';
  $code_continent = 'EU';
} else {
  $reader = new Reader('/usr/local/share/GeoIP/GeoLite2-Country.mmdb');
  $record = $reader->country($_SERVER['REMOTE_ADDR']);

  $code_country = $record->country->isoCode;
  $code_continent = $record->continent->code;
}

/* Mirrors. */
$mirror = get_mirror($code_country, $code_continent);
$mirrors = get_mirrors($mirror); // Pass the current mirror so it can be skipped from the list.
$mirrors_unique = mirrors_remove_duplicate($mirrors);

/* Prepend the mirror url to the filepath. */
$dl_url = $mirror['url'] . rtrim($filepath, '/');

/* Used for showing the name of the file about to download. */
$dl_filename = explode('/', rtrim($filepath, '/'));
$dl_filename = end($dl_filename);

/* Redirect to download URL. */
if ($do_redirect) {
  header('refresh:1;url=' . $dl_url);
}

// Benevolent Advertising.
$slates = get_field('slates');
$slates_geolocked = array();
$slates_worldwide = array();

foreach ($slates as $slate_number => $slate){
  if (!$slate or !$slate['is_enabled']) {
    continue;
  }

  $geo_availability = $slate['availability'];

  if ($geo_availability == 'geolocked') {
    // If country is defined and we are in it, show it.
    if ($slate['slate_country'] and $slate['slate_country'] == $code_country){
        array_push($slates_geolocked, $slate_number);

    // If continent is defined and we are in it, show it.
    } elseif ($slate['slate_continent'] and $slate['slate_continent'] == $code_continent){
        array_push($slates_geolocked, $slate_number);
    }
  }

  if ($geo_availability == 'worldwide') {
    array_push($slates_worldwide, $slate_number);
  }
}

$slates_all = array_merge($slates_worldwide, $slates_geolocked);
$slate_number = array_rand($slates_all);

$slate_to_show = $slates[$slates_all[$slate_number]];

?>

<?php
/* Header layout and style. */
$header_title          = get_field('header_title');
$header_subtitle       = get_field('header_subtitle');
$header_credits        = get_field('header_background_credits');
$header_image_offset   = get_field('header_image_offset');
$header_image_src      = get_the_post_thumbnail_url($post->ID, 'full');
$background_overlay    = get_field('background_overlay');

$header_align   = get_field('header_align');
$header_align_x = isset($header_align) ? $header_align['header_align_x'] : 'header-align-x-center';
$header_align_y = isset($header_align) ? $header_align['header_align_y'] : 'header-align-y-center';

get_header();?>

<?php

$is_blender = str_starts_with($filepath, 'release');
// If the filepath starts with "release", update the Blender downloads count.
if ($is_blender) {
    updateDownloadCount($dl_filename, $mirror['url']);
}

$dl_label = ($is_blender ? 'Blender' : $dl_filename);

?>

<section class="hero download thanks">
    <div class="container">
        <div class="hero-content">
         <?php if (isset($dl_url)): ?>
          <div class="download-status">
              <details>
                  <summary>
                      Your download will start automatically.
                      If it didn't start, <u>download manually</u>.
                  </summary>
                  <div class="py-3">
                    <p class="m-0">Direct link: <a href="<?=$dl_url?>"><?=$dl_url?></a></p>
                  </div>

                  Alternative Mirrors
                  <div class="d-alt-mirrors">
                    <ul class="list-inline">
                      <?php
                        foreach($mirrors_unique as $mirror_alt):
                        $alt_dl_url = $mirror_alt['url'] . rtrim($filepath, '/');
                        ?>
                        <li>
                          <a href="<?=$alt_dl_url?>" title="<?=$mirror_alt['name']?>">
                            <?=$mirror_alt['code_country']['label']?>
                          </a>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                  </div>
              </details>
          </div>
          <?php endif ?>

          <?=$slate_to_show['slate_content']?>
        </div>
    </div>

	<?php if ($header_image_src): ?>
	<div class="hero-background-faded">
		<div class="hero-background-faded-image" style="top: <?=$header_image_offset?>">
			<img src="<?=$header_image_src?>" alt=""/>
		</div>
	</div>
	<?php endif;?>

	<?php if ($background_overlay): ?>
	<div class="hero-overlay" style="background-color: <?=$background_overlay?>"></div>
	<?php endif; ?>

	<div class="hero-overlay hero-overlay-bottom"></div>
	<div class="hero-overlay hero-overlay-top"></div>
</section>

<?php while ( have_posts() ) : the_post(); ?>
  <?php if (!empty(get_the_content())): ?>
    <div class="container">
      <?=the_content()?>
    </div>
  <?php endif; ?>
<?php endwhile; ?>

<?php if ($do_redirect and !str_starts_with($filepath, 'thanks')): ?>
<script type="text/javascript">
    window.history.pushState(null, 'Thanks', '/thanks/');
</script>
<?php endif ?>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
