<?php get_header(); ?>
<?php if (get_bloginfo('name') != 'blender.org'){
	get_header('static');
}

$page_width = get_field('page_width');
?>

	<div class="blog-container">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php
				$post_status = get_post_status();

				$post_author = get_the_author();
				$post_author_url = get_author_posts_url(get_the_author_meta('id'));

				$is_guest_blogpost = get_field('is_guest_blog_post');
				if ($is_guest_blogpost) {
					$guest_author = get_field('guest_author');

					$post_author     = $guest_author['author_name'];
					$post_author_url = $guest_author['author_url'];
				}
			?>

			<div class="<?=($page_width ? $page_width : 'container')?>">
				<?php if ( !post_password_required() ): ?>

				<script type="application/ld+json">
					{
						"@context": "https://schema.org",
						"@type": "NewsArticle",
						"headline": "<?php the_title(); ?>",
						<?php if (has_post_thumbnail()): ?>"image": [
							"<?php the_post_thumbnail_url(); ?>"
						],<?php endif; ?>
						"datePublished": "<?php the_date('c'); ?>",
						"dateModified": "<?php the_modified_date('c'); ?>",
						"author": [{
								"@type": "Person",
								"name": "<?=$post_author?>",
								"url": "<?=$post_author_url?>"
							}]
					}
				</script>

				<?php if (is_preview()):?>
				<div class="row mt-3">
					<div class="col-md-8 mx-auto">
						<div class="cards-list cards-2">
							<div class="cards-list-item-outer">
								<div class="cards-list-item-inner">
									<div class="cards-list-item-thumbnail">
										<span class="cards-list-item-title mt-3">Preview</span>
									</div>
									<span class="cards-list-item-excerpt pb-3">
										This is how your post will look when listed.
										<br/>
										The featured image <strong>must be 16:9</strong>.
										<hr/>
										The excerpt is a 20-ish word version of the post.
										To write your own excerpt enable the panel in <i class="i-more-vertical"></i> (top-right corner),
										<em>Preferences</em>, <em>Panels</em>, while editing the post.
									</span>
								</div>
							</div>

							<div class="cards-list-item-outer">
								<div class="cards-list-item-inner">
									<?php if (has_post_thumbnail()): ?>
										<a href="<?php the_permalink(); ?>" class="cards-list-item-thumbnail">
											<?php the_post_thumbnail('featured-list', ['class' => 'img-fluid', 'title' => get_the_title()]); ?>
										</a>
									<?php endif; ?>
									<a class="cards-list-item-title" href="<?php the_permalink(); ?>"><?php the_title() ?></a>
									<a class="cards-list-item-excerpt" href="<?php the_permalink(); ?>"><?php the_excerpt();?></a>
									<div class="cards-list-item-extra">
										<ul>
											<li><a href="<?php the_permalink(); ?>"><?php the_time('F jS, Y'); ?></a></li>

											<li>
												<?php if ($is_guest_blogpost): ?>
													<a href="<?=$post_author_url?>" target="_blank"><?=$post_author?></a>
												<?php else: ?>
													<?php the_author_posts_link(); ?>
												<?php endif; ?>
											</li>

											<?php if (comments_open()): ?>
												<li class="comments right"><?php comments_popup_link('', '1 <i class="i-comment"></i>', '% <i class="i-comment"></i>'); ?></li>
											<?php endif; ?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr/>
				<?php endif; ?>
				<?php endif; ?>
				<div class="row blog">
					<div class="col-md-8 mx-auto">
						<article id="post-<?php the_ID(); ?> <?php post_class(); ?>">
							<?php if ( !post_password_required() ): ?>
							<div class="blog-header">
								<h1><?php the_title(); ?></h1>
								<ul>
									<li><?php the_time('F jS, Y'); ?></li>

									<?php if ($post_status != 'publish'): ?>
									<li title="Status" class="text-warning"><?=$post_status?></li>
									<?php endif; ?>

									<?php if (!empty($post->post_password)): ?>
									<li class="text-danger">Private</li>
									<?php endif; ?>

									<li><?php the_category(', '); ?></li>

									<li>
										<?php if ($is_guest_blogpost): ?>
											<a href="<?=$post_author_url?>" target="_blank"><?=$post_author?></a>
										<?php else: ?>
											<?php the_author_posts_link(); ?>
										<?php endif; ?>
									</li>
								</ul>
							</div><!-- blog_article_header -->
							<?php endif; ?>
							<div class="entry-content">
								<?php the_content(); ?>
							</div>
						</article>
						<div class="blog-navigation single">
							<?php previous_post_link('%link', '<i class="i-chevron-left"></i> <span>%title</span>'); ?>
							<?php next_post_link('%link', '<span>%title</span> <i class="i-chevron-right"></i>'); ?>
						</div>
					</div>
				</div> <!-- row blog  -->
			</div>
		</div>
		<div class="blog-bottom">
			<div class="container">
				<div class="row blog">
					<div class="col-md-8 mx-auto blog">
						<?php
							if (comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>
						<div class="blog-social">
							<span class="blog-social-text">SHARE THIS STORY ON</span>
							<ul>
								<li>
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
										<i class="i-facebook"/></i> Facebook
									</a>
								</li>
								<li>
									<a href="https://twitter.com/home?status=<?php the_title(); ?>&nbsp;@blender_org&nbsp;<?php the_permalink(); ?>&nbsp;%23b3d">
										<i class="i-twitter"/></i> X
									</a>
								</li>
								<li>
									<a href="http://www.reddit.com/submit?url=<?php the_permalink(); ?>">
										<i class="i-reddit"/></i> Reddit
									</a>
								</li>
								<li>
									<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&source=Blender">
										<i class="i-linkedin"/></i> LinkedIn
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endwhile; ?>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
<script>
	let url = window.location.href;

	/* Move reply box below comment being replied to. */
	if (url.indexOf("?replytocom") > -1) {
		let comment_id = url.substring(
    	url.indexOf("=") + 1,
    	url.lastIndexOf("#")
		);

		let comment = document.getElementById('comment-' + comment_id);
		let comment_reply_form = document.getElementById('comment-reply-form');

		comment.classList.add('is-replying-to');
		comment.insertAdjacentElement('beforeEnd', comment_reply_form);
	}
</script>
