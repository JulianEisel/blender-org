/* Redirect
 * Usage:
 * - element must have class 'js-redirect'
 * - element must have attribute 'data-url'
 * e.g.: <div class="js-redirect" data-url="https://cloud.blender.org">
*/
var jsRedirect = document.getElementsByClassName("js-redirect");

var redirect = function() {
	var url = this.getAttribute("data-url");
	window.location.href = url;
};

for (var i = 0; i < jsRedirect.length; i++) {
	jsRedirect[i].addEventListener('click', redirect, false);
}


// Get the highest resolution in a srcset - https://stackoverflow.com/a/60487971
function getHighestResImg(element){
  if(element.getAttribute('srcset')){
      let highResImgUrl = '';
      let maxRes = 0;
      let imgWidth,urlWidthArr;
      element.getAttribute('srcset').split(',').forEach((item)=>{
          urlWidthArr = item.trim().split(' ');
          imgWidth = parseInt(urlWidthArr[1]);
          if(imgWidth > maxRes) {
              maxRes = imgWidth;
              highResImgUrl = urlWidthArr[0];
          }
      });

      return highResImgUrl;
  }else{
    return  element.getAttribute('src');
  }
}


/* Lightbox
 * Usage:
 * - the element must have the class 'js-isolify' or
 * - select the "Expand on Click" style on WP image blocks.
 * - it should also have the following arguments:
 *   - data-url: Required. Pass the image source
 *   - data-alt: Optional
 *   - data-type: Optional. Currently only used by oembed to pass content
*/
var lightboxElements = document.querySelectorAll('.js-isolify, .js-lightbox, .is-style-js-isolify > a, .is-style-js-isolify > img');

function lightboxShow() {
	const type = this.getAttribute("data-type");
	let url = this.getAttribute("data-url");
	let caption = this.getAttribute("data-alt");

	// If it's a WP block, or a link, look for images inside.
	if (this.nodeName == 'A' || this.classList.contains('wp-block-image')){
		img = this.getElementsByTagName('img')[0];
	} else if (this.nodeName == 'IMG') {
		img = this;
	}

	if (img) {
		url = getHighestResImg(img);
		caption = img.getAttribute("alt");
	}

	// Build a temp container.
	let elLightbox = document.createElement("div");
	elLightbox.classList.add("lightbox-container");
	elLightbox.id = "lightbox-container";

	if (typeof type !== 'undefined' && type == 'oembed'){
		elLightbox.innerHTML = url;
	} else {
		let elImage = document.createElement("img");
		elImage.src = url;
		elImage.alt = caption;
		elLightbox.appendChild(elImage);
	}

	if (caption) {
		let elCaption = document.createElement("div");
		elCaption.classList.add("lightbox-caption");
		elCaption.innerHTML = caption;
		elLightbox.appendChild(elCaption);
	}

	let elUnderlay = lightboxCreateUnderlay();
	elLightbox.appendChild(elUnderlay);

	document.body.classList.add("is-lightbox-active");
	document.body.appendChild(elLightbox);
}

function lightboxHide() {
	const elLightbox = document.getElementById('lightbox-container');

	if (elLightbox !== null) {
		elLightbox.remove();
		document.body.classList.remove("is-lightbox-active");
	}
}

function lightboxCreateUnderlay() {
	let elUnderlay = document.createElement("div");;
	elUnderlay.classList.add("lightbox-underlay");

	elUnderlay.addEventListener('click', lightboxHide, true);
	return elUnderlay;
}

if (lightboxElements.length) {
	for (var i = 0; i < lightboxElements.length; i++) {
		lightboxElements[i].addEventListener('click', lightboxShow, false);

		// Prevent opening <a> links on isolated elements.
		if (lightboxElements[i].nodeName == 'A'){
			a = lightboxElements[i];
		} else if (lightboxElements[i].nodeName == 'IMG'){
			a = lightboxElements[i].parentNode.getElementsByTagName('a')[0];
		} else {
			a = lightboxElements[i].getElementsByTagName('a')[0];
		}

		// A link inside is when a WP image block links to "Media file".
		// Simply preventDefault, so MMB still opens in a new tab.
		if (a){ a.addEventListener('click', function(e){ e.preventDefault();});}
	}

	window.document.onkeydown = function (e) {
		if (e.key == "Escape") {
			lightboxHide();
		}
	}
}

function addClassInViewport(selector) {
	// Get all elements matching the selector.
	const targetElements = document.querySelectorAll(selector);
	if (targetElements.length == 0) {return;}

	let options = {
		rootMargin: "100px",
		threshold: 0.6
	};

	// Create a single Intersection Observer to observe all target elements.
	const observer = new IntersectionObserver(elements => {
		elements.forEach(element => {
			const targetElement = element.target;

			if (element.isIntersecting) {
				targetElement.classList.add('in-viewport');
			}
		});
	}, options);

	// Start observing all target elements.
	targetElements.forEach(element => {
		observer.observe(element);
	});
}

//- TODO (Pablo): Run only on demand via a page option or so.
addClassInViewport('.in-viewport-fade');
