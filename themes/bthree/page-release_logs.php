<?php
/*
Template Name: Release Notes (deprecated)
*/
?>
<?php
get_header();

$introduction_text      = get_field('introduction_text');
$splash                 = get_field('splash_image');
$splash_credits         = get_field('splash_credits');
$splash_artwork         = get_the_post_thumbnail_url($post->ID, 'full');

$introduction_sections  = get_field('introduction_sections');
$introduction_summary   = get_field('introduction_summary');

$current_version_full   = get_field('blender_version', 'option');
$current_version_number = preg_replace("/[^0-9,.]/", "", $current_version_full); // Strip letter from version (e.g. 2.77a > 2.77)
$version_full           = get_field('version_number');
$version_number         = preg_replace("/[^0-9,.]/", "", $version_full);
$is_latest = (($version_number >= $current_version_number) ? true : false);

?>
<?php get_header('static'); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php if ($introduction_text): ?>
<div class="container">
	<div class="featured-content box">
		<div class="row">
			<?php if ($splash): ?>
			<div class="col-md-6">
				<div
						class="js-isolify rounded cursor-pointer text-center"
						data-url="<?=$splash['url']?>"
						data-credits="<?=htmlspecialchars($splash_credits)?>">
					<img class="img-fluid rounded bg-dark mb-3" src="<?=$splash['url']?>" alt="Blender <?=$version_number?> Splash Screen"/>
				</div>
			</div>
			<?php endif; ?>
			<div class="<?=($splash ? 'col-md-6' : 'col-md-12')?>">
				<div class="px-3">
						<div class="px-3 border-bottom pb-3"><?=$introduction_text?></div>

						<div class="d-flex justify-content-center mt-4 align-items-center">
							<a class="mx-auto btn btn-primary d-block"
								title="Download the latest Blender (recommended)"
								href="<?=get_permalink(get_page_by_path('download'))?>">
								Download Latest Blender
							</a>

							<?php if (!$is_latest): ?>
								<a class="mx-auto"
									href="http://download.blender.org/release/Blender<?=$version_number?>/"
									title="Get the old version (not recommended)">
									Download Blender <?=$version_full?> (old)
								</a>
							<?php endif; ?>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<div class="release-logs">
	<?php if (!empty(get_the_content())): ?>
		<?php the_content(); ?>
	<?php endif; ?>

	<?php if ($introduction_summary): ?>
		<div class="container py-5">
			<div class="features__releaselogs_whatsnew py-3">
				<h1 class="text-overlay"><?=($introduction_sections ? $introduction_sections : 'New in Blender ' . $version_full)?></h1>
			</div>

			<div class="d-flex align-items-center flex-wrap justify-content-center features__releaselogs_index p-4">
				<?php while( have_rows('sections') ): the_row(); ?>

					<?php if (get_sub_field('section_enabled')): ?>
						<?php $section_title = get_sub_field('section_title'); ?>

						<a href="#<?=slugify($section_title)?>"><?=$section_title?></a>

					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>

		<?php

			$count = 1;

			while( have_rows('sections') ): the_row(); ?>
			<?php
				$section_enabled       = get_sub_field('section_enabled');
				$section_title         = get_sub_field('section_title');
				$section_description   = get_sub_field('section_description');
				$section_url           = get_sub_field('section_url');
				$section_category      = get_sub_field('section_category');
				$section_slug          = slugify($section_title);

				$section_image         = get_sub_field('section_image');
				$section_image_credits = get_sub_field('section_image_credits');

				/* Style. */
				$section_background    = get_sub_field('section_background');
				$section_bg_size       = get_sub_field('section_background_size');
				$section_color_bg      = get_sub_field('section_color_background');
				$section_color_text    = get_sub_field('section_color_text');

				$section_image_flip    = get_sub_field('section_image_flip');

				// If the section is enabled, draw the box and title
				if ($section_enabled):
				?>

			<div
				id="<?=$section_slug?>"
				class="features__releaselogs_section <?=($count%2==1 ? 'odd' : '')?> <?=$section_category?>"
				style="<?=(($section_background and $section_bg_size == 'full') ? 'background-image: url(' . $section_background['url'] . ');' : '')?>
				<?=($section_color_bg ? 'background-color: ' . $section_color_bg . ';' : '')?>
				<?=($section_color_text ? 'color: ' . $section_color_text . ';' : '')?>">

				<?php if ($section_bg_size != 'full'): ?>
				<div class="section-background bg-align-<?=$section_bg_size?>"><img src="<?=$section_background['url']?>" alt=""/></div>
				<?php endif; ?>

				<div class="container">

					<div class="features__releaselogs_section-box box-align-<?=$section_bg_size?>">

						<div class="features__releaselogs_section-intro <?=($section_image_flip ? 'section-flip' : '')?>">
							<?php if ($section_image): ?>
								<div class="features__releaselogs_section-thumbnail js-isolify"
										data-url="<?=$section_image['url']?>"
										data-credits="<?=htmlspecialchars($section_image_credits)?>">
									<img src="<?=$section_image['url']?>" alt="<?=$section_title?>"/>
								</div>
							<?php endif; ?>

							<div class="features__releaselogs_section-description">

								<?php if ($section_title): ?>
									<a href="#<?=$section_slug?>" class="features__releaselogs_section-title">
										<?=$section_title?>
										<span><i class="i-link"></i></span>
									</a>
								<?php endif; ?>

								<?php if ($section_description): ?>
									<div class="description-text"><?=$section_description?></div>
								<?php endif; ?>

								<?php if ($section_url): ?>
									<a class="read-more" href="<?=$section_url?>">
										Read more <i class="i-chevron-right"></i>
									</a>
								<?php endif; ?>
							</div>
						</div>

						<?php // If the section has some kind of artwork, show off ?>
						<?php if( have_rows('section_artwork') ):

							// loop through artwork types, and style appropriately
							while ( have_rows('section_artwork') ) : the_row(); ?>


								<?php if( get_row_layout() == 'gallery' ): ?>
								<div class="features__releaselogs_section-gallery mb-5">

									<?php while (have_rows('section_gallery')) : the_row(); ?>

										<?php if( get_row_layout() == 'gallery_image' ):
											$gallery_image_item = get_sub_field('gallery_image_item'); ?>

											<figure>
												<img
													alt="Gallery image"
													class="js-isolify"
													src="<?=$gallery_image_item['sizes']['medium']?>"
													data-url="<?=$gallery_image_item['url']?>"
													data-credits="<?=htmlspecialchars($gallery_image_item['caption'])?>"
													/>
											</figure>
										<?php endif; ?>

										<?php if( get_row_layout() == 'gallery_oembed' ):

											$gallery_oembed_thumbnail = get_sub_field('gallery_oembed_thumbnail');
											$gallery_oembed_source = get_sub_field('gallery_oembed_url'); ?>

											<figure>
												<i class="gallery-item-icon i-play"></i>
												<img
													alt="Gallery embed"
													class="js-isolify"
													src="<?=$gallery_oembed_thumbnail['sizes']['medium']?>"
													data-url="<?=htmlspecialchars($gallery_oembed_source)?>"
													data-type="oembed"
													data-credits="<?=$gallery_oembed_thumbnail['caption']?>"
													/>
											</figure>

										<?php endif; ?>
									<?php endwhile; ?>
								</div>
								<?php endif; // endif get_row_layout gallery ?>


								<?php if( get_row_layout() == 'compare_images' ):

									$section_compare_notes   = get_sub_field('section_compare_notes');
									$section_compare_left    = get_sub_field('section_compare_left');
									$section_compare_right   = get_sub_field('section_compare_right');
									$section_compare_credits = get_sub_field('section_compare_credits');
									?>
									<div class="mb-5">

									<?php if($section_compare_notes): ?>
										<div class="images-compare-notes">
											<?=$section_compare_notes?>
										</div>
									<?php endif; ?>
									<div class="images-compare">
										<div style="display: none;">
											<?php if ($section_compare_left['caption']): ?>
												<span class="images-compare-label">
													<?=$section_compare_left['caption']?>
												</span>
											<?php endif; ?>
											<img src="<?=$section_compare_left['sizes']['large']?>" alt="<?=$section_compare_left['caption']?>">
										</div>
										<div>
											<?php if ($section_compare_right['caption']): ?>
												<span class="images-compare-label">
													<?=$section_compare_right['caption']?>
												</span>
											<?php endif; ?>
											<img src="<?=$section_compare_right['sizes']['large']?>" alt="<?=$section_compare_right['caption']?>">
										</div>
									</div>
									<?php if($section_compare_credits): ?>
										<div class="images-compare-credits">
											<?=$section_compare_credits?>
										</div>
									<?php endif; ?>
									</div>
								<?php endif; // endif compare_images ?>


								<?php if( get_row_layout() == 'oembed' ): ?>
									<div class="embed-container">
										<?php the_sub_field('section_oembed'); ?>
									</div>
								<?php endif; // endif oembed ?>


								<?php if( get_row_layout() == 'text_block' ): ?>
									<div class="lead p-5">
										<?php the_sub_field('section_text_block'); ?>
									</div>
								<?php endif; // endif text_block ?>


								<?php if( get_row_layout() == 'link_cloud' ):
											$links = get_sub_field('section_links_cloud'); ?>
									<div class="lead p-5">
										<ul class="list-unstyled d-flex justify-content-center align-items-center flex-wrap text-center">
										<?php foreach ($links as $link_index => $link):

											$link_title = $link['link_title'];
											$link_url   = $link['link_url'];
											?>
											<li>
												<?php if ($link_url): ?>
													<a href="<?=$link_url?>" target="_blank" class="d-block px-3 py-2">
														<?=$link_title?>
													</a>
												<?php else: ?>
													<span class="d-block px-3 py-2"><?=$link_title?></span>
												<?php endif; ?>
											</li>
										<?php endforeach; ?>
										</ul>
									</div>
								<?php endif; // endif links_cloud ?>


								<?php if( get_row_layout() == 'tabs' ):

									$tabs = get_sub_field('tab_items');
								?>
								<div class="container mb-5">
									<ul class="nav nav-tabs mb-3 list-bullets-none justify-content-center" role="tablist">
										<?php foreach ($tabs as $tab_index => $tab):
											$tab_title       = $tab['tab_title'];
											$tab_description = $tab['tab_description'];
											$tab_image       = $tab['tab_image'];
											$tab_video       = $tab['tab_video'];
											$tab_video_attr  = $tab['tab_video_attr'];
										?>
										<li class="nav-item">
											<a	class="nav-link py-2 <?=($tab_index == 0 ? 'active' : '')?>"
													id="t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>-tab" data-toggle="pill" href="#t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>" role="tab"
													aria-controls="t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>" aria-selected="<?=($tab_index == 0 ? 'true' : 'false')?>">
												<?=$tabs[$tab_index]['tab_title']?>
											</a>
										</li>
										<?php
											endforeach; ?>
									</ul>
									<div class="tab-content text-center">
										<?php foreach ($tabs as $tab_index => $tab): ?>
											<div class="tab-pane <?=($tab_index == 0 ? 'show active' : '')?>" id="t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>" role="tabpanel" aria-labelledby="t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>-tab">
												<?php if($tabs[$tab_index]['tab_video']): ?>
													<div class="embed-responsive embed-responsive-16by9 rounded">
														<video class="embed-responsive-item" <?php echo implode(" ", $tab['tab_video_attr']);?> muted loop>
															<source src="<?=$tabs[$tab_index]['tab_video']?>" type="video/<?=substr($tabs[$tab_index]['tab_video'], -3)?>">
														</video>
													</div>
												<?php endif; ?>
												<?php if($tabs[$tab_index]['tab_image']): ?>
													<img src="<?=$tabs[$tab_index]['tab_image']['url']?>" class="img-fluid rounded" alt="<?=$tabs[$tab_index]['tab_title']?>">
												<?php endif; ?>
												<?php if($tabs[$tab_index]['tab_description']): ?>
													<div class="py-3 px-5 mx-5 lead"><?=$tabs[$tab_index]['tab_description']?></div>
												<?php endif; ?>
											</div>
										<?php endforeach; ?>
									</div>
								</div>
								<?php endif; // endif get_row_layout tabs ?>


								<?php if( get_row_layout() == 'list_items' ):

									$list_items_list    = get_sub_field('list_items_list');
									$list_items_compact = get_sub_field('list_items_compact');
									$list_items_intro   = get_sub_field('list_items_intro');
									$list_items_per_row = get_sub_field('list_items_per_row');
								?>
									<?php if ($list_items_intro): ?>
										<div class="lead text-center">
											<?=$list_items_intro?>
										</div>
									<?php endif; ?>

									<div class="cards-list cards-<?=($list_items_per_row ? $list_items_per_row : '4')?> <?=($list_items_compact ? 'compact' : '')?> mb-5">
										<?php foreach ($list_items_list as $list_item):

											$list_item_url        = $list_item['list_item_url'];
											$list_item_title      = $list_item['list_item_title'];
											$list_item_text       = $list_item['list_item_text'];
											$list_item_thumbnail  = $list_item['list_item_thumbnail'];

											$is_image_only        = !($list_item_title or $list_item_text);

											/* Style. */
											$list_item_color_text = $list_item['list_item_color_text'];
											$list_item_color_bg   = $list_item['list_item_color_bg'];
										?>

											<div class="cards-list-item-outer<?=($is_image_only ? ' is-image-only' : '')?>">
												<div class="cards-list-item-inner" style="<?=($list_item_color_bg ? 'background-color: ' . $list_item_color_bg . ';' : '')?><?=($list_item_color_text ? 'color: ' . $list_item_color_text . ';' : '')?>">

													<?php if ($list_item['list_item_thumbnail']): ?>
														<?php if ($list_item_url): ?>
														<a href="<?=$list_item_url?>" class="cards-list-item-thumbnail">
															<img src="<?=$list_item['list_item_thumbnail']['url']?>" alt="<?=$list_item_title?>">
														</a>
														<?php else: ?>
														<div class="cards-list-item-thumbnail">
															<img src="<?=$list_item['list_item_thumbnail']['url']?>" alt="<?=$list_item_title?>">
														</div>
														<?php endif; ?>
													<?php endif; ?>

													<?php if (!$is_image_only): ?>
													<div class="cards-list-item-info">
														<?php if ($list_item_title && $list_item_url): ?>
															<a class="cards-list-item-title" href="<?=$list_item_url?>"><?=$list_item_title?></a>
														<?php elseif ($list_item_title): ?>
															<span class="cards-list-item-title"><?=$list_item_title?></span>
														<?php endif; ?>

														<?php if ($list_item_text): ?>
															<span class="cards-list-item-excerpt" href="<?=$list_item_url?>"><?=$list_item_text?></span>
														<?php endif; ?>
													</div>
													<?php endif; ?>
												</div>
											</div>

										<?php endforeach; ?>
									</div>
								<?php endif; // endif list_items ?>

							<?php endwhile; // while there are layouts ?>

						<?php endif; // section_artwork ?>

					</div>
				</div>
			</div>

			<?php
				endif;
				$count++;
			?>
		<?php endwhile; // sections ?>

		<div id="major-feature" class="features__releaselogs_section major-feature py-5">
			<div class="container">
				<div class="features__releaselogs_section-box">
					<div class="features__releaselogs_section-intro ">
						<div class="features__releaselogs_section-description text-center">
							<span class="features__releaselogs_section-title">
								Stay up-to-date
							</span>
							<div class="description-text pb-3">
								<div class="row pb-3">
									<div class="col-md-6">
										<a href="https://www.youtube.com/playlist?list=PLa1F2ddGya_87HJ72v_IgKUTNLIXSMfvB"><h3 class="my-3">Weekly Live Streams</h3></a>
										<div class="embed-responsive embed-responsive-16by9 rounded">
											​<iframe width="560" height="315" class="embed-responsive-item" src="https://www.youtube.com/embed/videoseries?list=PLa1F2ddGya_87HJ72v_IgKUTNLIXSMfvB" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
										</div>
									</div>
									<div class="col-md-6">
										<a href="https://www.youtube.com/playlist?list=PLyGoWAGEPJ1DLzhm1_dhuLVe7wT1kBFNP"><h3 class="my-3">Development Videos</h3></a>
										<div class="embed-responsive embed-responsive-16by9 rounded">
											​<iframe width="560" height="315" class="embed-responsive-item" src="https://www.youtube.com/embed/videoseries?list=PLyGoWAGEPJ1DLzhm1_dhuLVe7wT1kBFNP" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
										</div>
									</div>
								</div>

								<hr/>

								<small class="text-muted">Follow the official hashtag for Blender stuff on social media <a href="https://twitter.com/search?q=%23b3d" target="_blank">#b3d</a></small>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="features__releaselogs_section-box box py-5">
					<div class="features__releaselogs_section-description py-0 px-3">
						<div class="row">
							<div class="col-md-8">
								<a href="https://fund.blender.org/" class="text-secondary">
									<h2 class="py-2">Blender Development Fund</h2>
								</a>
								<p class="lead">
									The donation program to support maintaining and improving Blender, for everyone.
									<a href="https://fund.blender.org/">Become a member for as little as €5 per month.</a>
								</p>
							</div>
							<div class="col-md-4 text-center display-1">
								<a href="https://fund.blender.org/" class="text-danger h-100 justify-content-center align-items-center d-flex text-decoration-none">
									<i class="i-heart"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<?php endwhile; // end of the loop. ?>

<?php

wp_enqueue_script('hammer', get_template_directory_uri() . '/assets/js/hammer-2.0.8.min.js');

// The media-compare script was modified to work with jQuery 3 (basically replacing .load and .error with .on('load', ...))
wp_enqueue_script('media-compare', get_template_directory_uri() . '/assets/js/jquery.images-compare-0.2.5.js');

function js_footer(){
?>
<script type='text/javascript'>
	$(document).ready(function() {
		$('.images-compare').imagesCompare();
	});
</script>

<?php }
add_action('wp_footer', 'js_footer');
?>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
