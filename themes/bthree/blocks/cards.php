<?php

/**
 * Cards Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create string based on the block name, stripping the 'acf/'' bit.
$className = $block['name'];
$className = explode('/', $className);
$className = $className[1];

// Create id attribute allowing for custom "anchor" value.
$id = $className . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
}

// Convert WP align values into Bootstrap float-left/right.
if( !empty($block['align']) ) {
	$className .= ' float-' . $block['align'];
}

// Load values and assign defaults.
$card_layout        = get_field('card_layout');
$cards_aspect_ratio = get_field('card_aspect_ratio');
$cards_list         = get_field('cards_list');
$cards_per_row      = get_field('cards_per_row');

/* Generic styling. */
$show_block       = get_field('show_block');
$background_color = get_field('background_color');
$text_color       = get_field('text_color');
$style            = get_field('style');
?>
<?php if (!isset($show_block) or $show_block): ?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

	<div class="cards-list card-layout-<?=$card_layout?> card-aspect-ratio-<?=$cards_aspect_ratio?>">
		<style>
			#<?=$id?> .cards-list {
				--cards-list-items-per-row: <?=($cards_per_row ? $cards_per_row : '4')?>;
			}
		</style>
		<?php foreach ($cards_list as $card_item):

			$card_enabled       = $card_item['card_enabled'];
			$card_url           = $card_item['card_url'];
			$card_headline      = $card_item['card_headline'];
			$card_title         = $card_item['card_title'];
			$card_text          = $card_item['card_text'];
			$card_thumbnail     = $card_item['card_thumbnail'];
			$card_thumbnail_url = $card_thumbnail ? $card_item['card_thumbnail']['url'] : null;
			$card_aspect_ratio  = $card_item['card_aspect_ratio'];
			$card_width         = $card_item['card_width'];

			$is_image_only   = !($card_title or $card_text);

			/* Style. */
			$card_color_text = $card_item['card_color_text'];
			$card_color_bg   = $card_item['card_color_bg'];
		?>

		<?php if ($card_enabled): ?>
			<div class="cards-list-item-outer<?=($is_image_only ? ' is-image-only' : '')?> card-aspect-ratio-<?=$card_aspect_ratio?>" <?=($card_width ? 'style="grid-column: span ' . $card_width . '"' : '')?>>
				<div class="cards-list-item-inner" style="<?=($card_color_bg ? 'background-color: ' . $card_color_bg . ';' : '')?><?=($card_color_text ? 'color: ' . $card_color_text . ';' : '')?>">

					<?php if ($card_thumbnail): ?>
						<?php if ($card_url): ?>
						<a href="<?=$card_url?>" class="cards-list-item-thumbnail">
							<?=mediaThumbnail($card_thumbnail_url, $card_title)?>
						</a>
						<?php else: ?>
						<div class="cards-list-item-thumbnail">
							<?=mediaThumbnail($card_thumbnail_url, $card_title)?>
						</div>
						<?php endif; ?>
					<?php endif; ?>

					<?php if (!$is_image_only): ?>
					<div class="cards-list-item-info">
						<?php if ($card_headline): ?>
							<a class="cards-list-item-headline" href="<?=$card_url?>"><?=$card_headline?></a>
						<?php endif; ?>
						<?php if ($card_title && $card_url): ?>
							<a class="cards-list-item-title" href="<?=$card_url?>"><?=$card_title?></a>
						<?php elseif ($card_title): ?>
							<span class="cards-list-item-title"><?=$card_title?></span>
						<?php endif; ?>

						<?php if ($card_text): ?>
							<span class="cards-list-item-excerpt" href="<?=$card_url?>"><?=$card_text?></span>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<?php endforeach; ?>
	</div>
  <?php if ($style or $background_color or $text_color): ?>
    <style type="text/css">
      #<?=$id?> {
        background: <?=$background_color?>;
        color: <?=$text_color?>;
      }
      <?=($style ? $style : '')?>
    </style>
  <?php endif; ?>
</div>
<?php endif; ?>
