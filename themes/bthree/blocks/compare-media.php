<?php

/**
 * Compare Media Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create string based on the block name, stripping the 'acf/'' bit.
$className = $block['name'];
$className = explode('/', $className);
$className = $className[1];

// Create id attribute allowing for custom "anchor" value.
$id = $className . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
}

// Convert WP align values into Bootstrap float-left/right.
if( !empty($block['align']) ) {
	$className .= ' float-' . $block['align'];
}

// Load values and assign defaults.
$compare_left_label  = get_field('compare_left_label');
$compare_left_type   = get_field('compare_left_type');
$compare_left_image  = get_field('compare_left_image');
$compare_left_video  = get_field('compare_left_video');

$compare_right_label = get_field('compare_right_label');
$compare_right_type  = get_field('compare_right_type');
$compare_right_image = get_field('compare_right_image');
$compare_right_video = get_field('compare_right_video');

$compare_credits     = get_field('compare_credits');

/* Generic styling. */
$show_block       = get_field('show_block');
$background_color = get_field('background_color');
$text_color       = get_field('text_color');
$style            = get_field('style');
?>
<?php if (!isset($show_block) or $show_block): ?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
	<div class="images-compare">
		<div style="display: none;">
			<?php if ($compare_left_label): ?>
				<span class="images-compare-label"><?=$compare_left_label?></span>
			<?php endif; ?>

			<?php if ($compare_left_type == 'video'): ?>
				<video muted loop autoplay src="<?=$compare_left_video?>" type="video/<?=substr($compare_left_video, -3)?>">
				</video>
			<?php else: ?>
				<img src="<?=$compare_left_image['sizes']['large']?>" alt="<?=$compare_left_label?>">
			<?php endif; ?>
		</div>

		<div>
			<?php if ($compare_right_label): ?>
				<span class="images-compare-label"><?=$compare_right_label?></span>
			<?php endif; ?>

			<?php if ($compare_right_type == 'video'): ?>
				<video muted loop autoplay src="<?=$compare_right_video?>" type="video/<?=substr($compare_right_video, -3)?>">
				</video>
			<?php else: ?>
				<img src="<?=$compare_right_image['sizes']['large']?>" alt="<?=$compare_right_label?>">
			<?php endif; ?>
		</div>
	</div>

	<?php if($compare_credits): ?>
		<div class="images-compare-credits">
			<?=$compare_credits?>
		</div>
	<?php endif; ?>

  <?php if ($style or $background_color or $text_color): ?>
    <style type="text/css">
			<?php
				/* Use the background color setting for each side. Helps to fix the issue when using transparent images
				* and the right-side label is not covered by the left side.
				*/
			?>
			#<?=$id?> .images-compare-before,
			#<?=$id?> .images-compare-after {
				background: <?=$background_color?>;
			}
			#<?=$id?> span.images-compare-label { color: <?=$text_color?>; }
			<?=($style ? $style : '')?>
		</style>
  <?php endif; ?>
</div>

<?
if (!function_exists('js_footer')) {
	wp_enqueue_script('hammer', get_template_directory_uri() . '/assets/js/hammer-2.0.8.min.js');

	// The images-compare script was modified to work with jQuery 3 (basically replacing .load and .error with .on('load', ...))
	wp_enqueue_script('media-compare', get_template_directory_uri() . '/assets/js/jquery.images-compare-0.2.5.js');

	function js_footer(){
	?>
	<script type='text/javascript'>
		$(document).ready(function() {
			$('.images-compare').imagesCompare();
		});
	</script>

	<?php }
	add_action('wp_footer', 'js_footer');
}

?>
<?php endif; ?>
