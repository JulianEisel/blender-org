<?php

/**
 * Video Scroll Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create string based on the block name, stripping the 'acf/'' bit.
$className = $block['name'];
$className = explode('/', $className);
$className = $className[1];

// Create id attribute allowing for custom "anchor" value.
$id = $className . '-' . $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}

// Convert WP align values into Bootstrap float-left/right.
if (!empty($block['align'])) {
  $className .= ' float-' . $block['align'];
}

// Load values and assign defaults.
$video_scroll          = get_field('video_scroll');
$scroll_threshold      = get_field('scroll_threshold');
$playback_time_stretch = (get_field('playback_time_stretch') * 10);

/* Generic styling. */
$show_block       = get_field('show_block');
$background_color = get_field('background_color');
$text_color       = get_field('text_color');
$style            = get_field('style');

global $block_id;
$block_id = esc_attr($id);
?>
<?php if (!isset($show_block) or $show_block) : ?>
  <?php if (!$video_scroll) {
    return;
  } ?>
  <?php
  $video_url = $video_scroll['url'];
  $video_ext = explode('.', $video_url);
  $video_ext = end($video_ext);
  ?>

  <div id="<?= $block_id ?>" class="<?php echo esc_attr($className); ?>">
    <video muted preload="metadata">
      <source src="<?= $video_url ?>" type="video/<?= $video_ext ?>" />
    </video>

    <?php if ($style or $background_color) : ?>
      <style type="text/css">
        <?= ($style ? $style : '') ?>#<?= $block_id ?> {
          background: <?= $background_color ?>;
        }
      </style>
    <?php endif; ?>

    <?php
    $script = '<script>';
    $script .= 'const blockId = "' . $block_id . '";';
    $script .= 'const playbackTimeStretch = ' . $playback_time_stretch . ';';
    $script .= 'const scrollThreshold = ' . $scroll_threshold . ';';
    $script .= '</script>';
    echo $script;
    ?>

    <style>
      .video-scroll {
        display: block;
        left: 0;
        min-height: 100px;
        overflow: hidden;
        position: sticky;
        top: 0;
        z-index: -1;
      }

      .video-scroll video {
        /* Full height of the viewport, and centered. */
        height: 100vh;
        left: 50%;
        position: absolute;
        transform: translateX(-50%);
      }

      .is-hidden {
        opacity: 0;
        position: relative;
        visibility: hidden;
      }

      /* Unset all positioning while in the editor. */
      .edit-post-visual-editor .video-scroll,
      .edit-post-visual-editor .video-scroll video {
        height: unset;
        left: unset;
        overflow: unset;
        position: unset;
        transform: unset;
      }
    </style>

    <script>
      document.addEventListener('DOMContentLoaded', function() {
        const container = document.getElementById(blockId);
        const videoElement = container.getElementsByTagName('video')[0];
        const videoHeight = videoElement.clientHeight;
        let containerHeight;
        let isVideoPlaying = false;
        let frameNumber = 0;

        /* When the video's metadata is loaded, set the
         * height of the video container to be the duration
         * multiplied by the playback time stretch setting. */
        videoElement.addEventListener('loadedmetadata', function() {
          containerHeight = (videoElement.duration - scrollThreshold) * playbackTimeStretch;
          container.style.height = containerHeight + "px";
        });

        const observer = new IntersectionObserver(entries => {
          const entry = entries[0];
          if (entry.isIntersecting) {
            isVideoPlaying = true;
            window.addEventListener('scroll', scrollPlay);
          } else {
            isVideoPlaying = false;
            window.removeEventListener('scroll', scrollPlay);
          }
        });

        observer.observe(container);

        function scrollPlay() {
          if (!isVideoPlaying) {
            return;
          }

          let containerOffsetTop = container.getBoundingClientRect().top;
          let nextElementOffsetTop = container.nextElementSibling.getBoundingClientRect().top;
          let videoOffsetTop = (videoHeight - nextElementOffsetTop) * -1;

          if (containerOffsetTop >= 0 && containerOffsetTop <= containerHeight) {
            frameNumber = Math.abs((containerHeight - nextElementOffsetTop) / playbackTimeStretch);
            videoElement.currentTime = frameNumber;
          }

          /* If we are close to the next element (the offset of the
           * next element is less than the height of the video),
           * start moving the video up accordingly. */
          if (nextElementOffsetTop < videoHeight) {
            videoElement.style.top = videoOffsetTop + 'px';
          } else {
            videoElement.style.top = '0px';
          }

          /* If the next element is fully in view,
           * hide the video container. */
          if (nextElementOffsetTop < 0) {
            container.classList.add('is-hidden');
          } else {
            container.classList.remove('is-hidden');
          }
        }

        requestAnimationFrame(scrollPlay); // Continue animation.
      });
    </script>
  </div>
<?php endif; ?>
