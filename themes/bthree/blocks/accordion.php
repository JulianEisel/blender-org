<?php

/**
 * Accordion Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create string based on the block name, stripping the 'acf/'' bit.
$className = $block['name'];
$className = explode('/', $className);
$className = $className[1];

// Create id attribute allowing for custom "anchor" value.
$id = $className . '-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['className']) ) {
  $className .= ' ' . $block['className'];
}

// Convert WP align values into Bootstrap float-left/right.
if( !empty($block['align']) ) {
  $className .= ' float-' . $block['align'];
}

/* Generic styling. */
$show_block       = get_field('show_block');
$background_color = get_field('background_color');
$text_color       = get_field('text_color');
$style            = get_field('style');
?>
<?php if (!isset($show_block) or $show_block): ?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?> wp-block-accordion">

  <?php while( have_rows('accordion_list') ): the_row();

    $accordion_title   = get_sub_field('accordion_title');
    $accordion_content = get_sub_field('accordion_content');
    $accordion_id      = get_row_index();
    $accordion_slug    = slugify($accordion_title);
    $open_status       = get_sub_field('open_status');
  ?>

  <details <?=($open_status ? 'open' : '')?> id="<?=$accordion_slug?>-<?=$accordion_id?>" style="<?=($background_color ? 'background-color: ' . $background_color . ';' : '')?>">
    <summary>
      <?=$accordion_title?>
    </summary>
    <?=$accordion_content?>
  </details>
  <?php endwhile; ?>

  <?php if ($style or $background_color or $text_color): ?>
    <style type="text/css">
      <?php if ($background_color or $text_color): ?>
      #<?=$id?> {
        color: <?=$text_color?>;
      }
      <?php endif; ?>
      <?=($style ? $style : '')?>
    </style>
  <?php endif; ?>

</div>
<?php endif; ?>
