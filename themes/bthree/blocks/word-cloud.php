<?php

/**
 * Word Cloud Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create string based on the block name, stripping the 'acf/'' bit.
$className = $block['name'];
$className = explode('/', $className);
$className = $className[1];

// Create id attribute allowing for custom "anchor" value.
$id = $className . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
}

// Convert WP align values into Bootstrap float-left/right.
if( !empty($block['align']) ) {
	$className .= ' float-' . $block['align'];
}

// Load values and assign defaults.
$words      = get_field('words') ?: 'List of words...';
$categories = get_field('categories') ?: 'List of categories...';
$columns    = get_field('column_count') ?: '2';

/* Generic styling. */
$show_block       = get_field('show_block');
$background_color = get_field('background_color');
$text_color       = get_field('text_color');
$style            = get_field('style');
?>
<?php if (!isset($show_block) or $show_block): ?>
<div id="<?php echo esc_attr($id); ?>" class="block-<?php echo esc_attr($className); ?>">

	<?php if (is_array($words)): ?>
	<ul class="list-unstyled d-flex justify-content-center align-items-center flex-wrap text-center">
	<?php foreach ($words as $word_index => $word):

		$word_title  = $word['title'];
		$word_url    = $word['url'];
		$is_strong   = $word['strong'];
		$is_external = $word['external'];
		?>
		<?php if ($word_title): ?>
			<li>
				<?php if ($word_url): ?>
					<a href="<?=$word_url?>" <?=($is_external ? 'target="_blank"' : '')?> class="d-block px-3 py-2">
						<?=($is_strong ? '<strong>' : '')?>
							<?=$word_title?>
						<?=($is_strong ? '</strong>' : '')?>
					</a>
				<?php else: ?>
					<span class="d-block px-3 py-2"><?=$word_title?></span>
				<?php endif; ?>
			</li>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>

	<?php if (is_array($categories)): ?>
	<div class="block-words-cloud-categories" <?=($columns ? 'style="column-count: ' . $columns . ';"': '')?>>
	<?php
	foreach ($categories as $category_index => $category):

		$category_title = $category['category_title'];
		$category_words = $category['words'];
		?>
		<div>
			<?php
			if ($category_title) {
				echo '<div class="category-title">' . $category['category_title'] . '</div>';
			}
			?>

			<ul>
			<?php
			foreach ($category_words as $word_index => $word):
				$word_title  = $word['title'];
				$word_url    = $word['url'];
				$is_strong   = $word['strong'];
				$is_external = $word['external'];
				?>
				<?php if ($word_title): ?>
				<li>
					<?=($is_strong ? '<strong>' : '')?>
					<?php if ($word_url): ?>
						<a href="<?=$word_url?>" <?=($is_external ? 'target="_blank"' : '')?>>
							<?=$word_title?>
						</a>
					<?php else: ?>
						<span><?=$word_title?></span>
					<?php endif; ?>
					<?=($is_strong ? '</strong>' : '')?>
				</li>
				<?php endif; ?>
			<?php endforeach; ?>
			</ul>
		</div>
	<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if ($style or $background_color or $text_color): ?>
		<style type="text/css">
			#<?=$id?> {
				color: <?=$text_color?>;
			}
			#<?=$id?> .block-words-cloud-categories > div {
				background: <?=$background_color?>;
			}
			<?=($style ? $style : '')?>
		</style>
	<?php endif; ?>
</div>
<?php endif; ?>
