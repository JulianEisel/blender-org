<?php

/**
 * Pages List Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create string based on the block name, stripping the 'acf/'' bit.
$className = $block['name'];
$className = explode('/', $className);
$className = $className[1];

// Create id attribute allowing for custom "anchor" value.
$id = $className . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
}

// Convert WP align values into Bootstrap float-left/right.
if( !empty($block['align']) ) {
	$className .= ' float-' . $block['align'];
}

// Load values and assign defaults.
$card_layout       = get_field('card_layout');
$card_aspect_ratio = get_field('card_aspect_ratio');
$cards_list        = get_field('cards_list');
$cards_per_row     = get_field('cards_per_row');

/* Generic styling. */
$show_block       = get_field('show_block');
$background_color = get_field('background_color');
$text_color       = get_field('text_color');
?>
<?php if (!isset($show_block) or $show_block): ?>
<div id="<?php echo esc_attr($id); ?>" class="cards-list card-layout-<?=$card_layout?> card-aspect-ratio-<?=$card_aspect_ratio?> cards-<?=($cards_per_row ? $cards_per_row : '3')?> <?php echo esc_attr($className); ?>">
	<?php while( have_rows('pages') ): the_row(); ?>
		<?php

			$pages = get_sub_field('page');
			global $post; ?>

		<?php if($pages): ?>

			<?php foreach($pages as $post): ?>
				<?php setup_postdata($post); ?>

					<div class="cards-list-item-outer">
						<div class="cards-list-item-inner">
							<?php if (has_post_thumbnail()): ?>
								<a href="<?php the_permalink(); ?>" class="cards-list-item-thumbnail">
									<?php the_post_thumbnail('featured-list', ['class' => 'img-fluid', 'title' => get_the_title()]); ?>
								</a>
							<?php endif; ?>

							<div class="cards-list-item-info">
								<a class="cards-list-item-title" href="<?php the_permalink(); ?>">
									<?php the_title() ?>
								</a>

								<a class="cards-list-item-excerpt" href="<?php the_permalink(); ?>"><?php the_excerpt();?></a>

								<div class="cards-list-item-more">
									<a class="more-item url" href="<?php the_permalink(); ?>">READ MORE  <i class="i-chevron-right"></i></a>
								</div>
							</div>

							<?php if ($background_color || $text_color) { ?>
							<style type="text/css">
								#<?php echo $id; ?> .cards-list-item-inner {
									background: <?php echo $background_color; ?>;
									color: <?php echo $text_color; ?>;
								}
							</style>
							<?php } ?>
						</div>
					</div>
				<?php break; ?>
				<?php wp_reset_postdata(); ?>
				<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		<?php endif; ?>

	<?php endwhile; ?>
</div>
<?php endif; ?>