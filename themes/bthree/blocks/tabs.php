<?php

/**
 * Compare Images Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create string based on the block name, stripping the 'acf/'' bit.
$className = $block['name'];
$className = explode('/', $className);
$className = $className[1];

// Create id attribute allowing for custom "anchor" value.
$id = $className . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
}

// Convert WP align values into Bootstrap float-left/right.
if( !empty($block['align']) ) {
	$className .= ' float-' . $block['align'];
}

// Load values and assign defaults.
$tabs = get_field('tab_items');

/* Generic styling. */
$show_block       = get_field('show_block');
$background_color = get_field('background_color');
$text_color       = get_field('text_color');
$style            = get_field('style');
?>
<?php if (!isset($show_block) or $show_block): ?>
<div id="<?=esc_attr($id); ?>" class="<?=esc_attr($className); ?>">
	<div>
		<ul class="nav nav-tabs" role="tablist">
			<?php foreach ($tabs as $tab_index => $tab):
				$tab_title        = $tab['tab_title'];
				$tab_description  = $tab['tab_description'];
				$tab_image        = $tab['tab_image'];
				$tab_video        = $tab['tab_video'];
				$tab_video_attr   = $tab['tab_video_attr'];
			?>
			<li class="nav-item">
				<a class="nav-link <?=($tab_index == 0 ? 'active' : '')?>"
						id="t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>-tab" data-toggle="pill" href="#t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>" role="tab"
						aria-controls="t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>" aria-selected="<?=($tab_index == 0 ? 'true' : 'false')?>">
					<?=$tabs[$tab_index]['tab_title']?>
				</a>
			</li>
			<?php
				endforeach; ?>
		</ul>
		<div class="tab-content text-center">
			<?php foreach ($tabs as $tab_index => $tab): ?>
				<div class="tab-pane <?=($tab_index == 0 ? 'show active' : '')?>" id="t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>" role="tabpanel" aria-labelledby="t-<?=slugify($tabs[$tab_index]['tab_title'])?>-<?=$tab_index?>-tab">
					<?php if($tabs[$tab_index]['tab_video']): ?>
						<div class="embed-responsive embed-responsive-16by9">
							<video class="embed-responsive-item rounded" <?=implode(" ", $tab['tab_video_attr']);?> muted loop <?php if($tabs[$tab_index]['tab_video_poster']){ echo ' poster="' . $tabs[$tab_index]['tab_video_poster']['url'] . '"'; }?>>
								<source src="<?=$tabs[$tab_index]['tab_video']?>" type="video/<?=substr($tabs[$tab_index]['tab_video'], -3)?>">
							</video>
						</div>
					<?php endif; ?>
					<?php if($tabs[$tab_index]['tab_image']): ?>
						<img src="<?=$tabs[$tab_index]['tab_image']['url']?>" class="img-fluid rounded" alt="<?=$tabs[$tab_index]['tab_title']?>">
					<?php endif; ?>
					<?php if($tabs[$tab_index]['tab_description']): ?>
						<div class="tab-description"><?=$tabs[$tab_index]['tab_description']?></div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>

  <?php if ($style or $background_color or $text_color): ?>
		<style type="text/css">
			#<?=$id?> {
				color: <?=$text_color?>;
				background-color: <?=$background_color?>;
			}
			#<?=$id?> .embed-responsive-item,
			#<?=$id?> img {
				background: <?=$background_color?>;
			}
			<?=($style ? $style : '')?>
		</style>
  <?php endif; ?>
</div>
<?php endif; ?>
