<?php

/**
 * Paragraph+ Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create string based on the block name, stripping the 'acf/'' bit.
$className = $block['name'];
$className = explode('/', $className);
$className = $className[1];

// Create id attribute allowing for custom "anchor" value.
$id = $className . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

$block_id = '#' . $id;

// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
}

// Convert WP align values into Bootstrap float-left/right.
if( !empty($block['align']) ) {
	$className .= ' float-' . $block['align'];
}

/* Load values and assign defaults. */
$paragraph_text = get_field('text');
$font_size      = get_field('font_size');

/* Paragraph style. */
$paragraph_style = get_field('paragraph_style') ?: 'em';

/* Gradient. */
$gradient_color_1 = get_field('gradient_color_1') ?: 'currentColor';
$gradient_color_2 = get_field('gradient_color_2') ?: 'currentColor';
$gradient_angle   = get_field('gradient_angle') ?: '45';
$gradient_align   = get_field('gradient_alignment') ?: '0';

/* <em> Styling. */
$em_text_color = get_field('em_text_color');
$em_background_color = get_field('em_background_color');
$em_style = get_field('em_style');

/* Generic styling. */
$show_block       = get_field('show_block');
$background_color = get_field('background_color');
$text_color       = get_field('text_color');
$style            = get_field('style');
?>
<?php if (!isset($show_block) or $show_block): ?>
<div id="<?php echo esc_attr($id); ?>" class="wp-block-<?php echo esc_attr($className); ?>">
	<?=$paragraph_text?>

	<style type="text/css">
		<?=$block_id?> {
			<?=($background_color ? 'background: ' . $background_color . ';' : '' )?>
			<?=($text_color ? 'color: ' . $text_color . ';' : '' )?>
			<?=($font_size ? 'font-size: ' . $font_size . ';' : '' )?>
		}

		<?php if ($paragraph_style == 'gradient'):
			$gradient = "linear-gradient(" . $gradient_angle . "deg, " . $gradient_color_1 . " " . $gradient_align . "%, " . $gradient_color_2 . ")";
			?>
		
			<?=$block_id?> {
				background-image: <?=$gradient?>;
				-webkit-background-clip: text;
				-webkit-text-fill-color: transparent;
			}
		<?php endif;?>

		<?php if ($paragraph_style == 'em'): ?>
			<?=$block_id?> em {
				<?=($em_background_color ? 'background: ' . $em_background_color . ';' : '' )?>
				<?=($em_text_color ? 'color: ' . $em_text_color . ';' : '' )?>
				<?=($em_style ? 'font-style: ' . $em_style . ';' : '' )?>
			}
		<?php endif;?>

		<?=($style ? $style : '')?>
	</style>
</div>
<?php endif;?>