<?php
/*
Template Name: FAQ (deprecated)
*/
?>

<?php get_header(); ?>
<?php get_header('static'); ?>

<?php
	$faq = get_field('faq');
?>

<div class="container py-4">

	<?php while ( have_posts() ) : the_post(); ?>
		<?php if (!empty(get_the_content())): ?>
			<?=the_content()?>
		<?php endif; ?>
	<?php endwhile; ?>

	<div class="row mb-4">
		<div class="col-md-12">
			<?php
				/* FAQ  */
				if ($faq): ?>

				<div class="box">
					<?php while( have_rows('faq') ): the_row();
						$faq_category      = get_sub_field('faq_category');
						$faq_category_slug = slugify($faq_category);
					?>
						<h3 id="<?=$faq_category_slug?>" class="pt-4 has-anchor border-top">
							<?=$faq_category?>
							<a href="#<?=$faq_category_slug?>" class="text-muted">
								<i class="i-link anchor-icon mr-3 text-primary"></i>
							</a>
						</h3>
						<div id="faq" class="py-3 mt-3 accordion">
							<?php while( have_rows('faq_questions_repeater') ): the_row();
								$faq_question = get_sub_field('faq_question');
								$faq_answer   = get_sub_field('faq_answer');
								$faq_id       = get_row_index();
							?>
							<div class="card mb-2" id="<?=$faq_category_slug?>-<?=$faq_id?>">
								<div class="cursor-pointer has-anchor" id="heading-<?=$faq_category_slug?>-<?=$faq_id?>">
									<h5 class="p-3 m-0" data-toggle="collapse" data-target="#faq-<?=$faq_category_slug?>-<?=$faq_id?>" aria-expanded="true" aria-controls="faq-<?=$faq_category_slug?>-<?=$faq_id?>">
										<i class="i-chevron-down"></i>
										<?=$faq_question?>
										<a href="#<?=$faq_category_slug?>-<?=$faq_id?>">
											<i class="i-link anchor-icon mx-2 text-primary"></i>
										</a>
									</h5>
								</div>
								<div id="faq-<?=$faq_category_slug?>-<?=$faq_id?>" class="collapse <?=(($faq_id == 1) ? 'show' : '')?>" aria-labelledby="heading-<?=$faq_id?>-<?=$faq_id?>" data-parent="#faq">
									<div class="card-body px-5 pb-2">
										<?=$faq_answer?>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endwhile; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>

</div>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
