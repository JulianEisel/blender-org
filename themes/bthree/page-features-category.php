<?php
/*
Template Name: Features: Category
*/
?>

<?php get_header(); ?>
<?php get_header('static'); ?>

<?php

$sub_categories = get_field('sub_categories');

$dev_status = [
	"new" => array(
		"title" => 'NEW!',
		"desc" => 'This is a fresh new feature!'
	),
	"upcoming" => array(
		"title" => 'UPCOMING',
		"desc" => 'This feature will be part of the next release!'
	),
	"deprecated" => array(
		"title" => 'DEPRECATED',
		"desc" => "This feature won't part of the next release"
	)
];
?>

<?php while ( have_posts() ) : the_post(); ?>
	<?php if (!empty(get_the_content())): ?>
		<?=the_content()?>
	<?php endif; ?>
<?php endwhile; ?>

<div class="container features-category">
	<div class="row">

		<?php /* SIDE NAVIGATION*/ ?>
		<?php if ($sub_categories): ?>
			<div class="col-sm-4">
				<nav id="features-nav" class="features-nav" data-spy="affix" data-offset-top="200" data-offset-bottom="180">
					<ul class="nav" role="tablist">
						<?php while( have_rows('sub_categories') ): the_row();
							$subcat_title   = get_sub_field('sub_category_title');
							$subcat_slug    = slugify($subcat_title);
							$subcat_details = get_sub_field('feature_details');
							$subcat_dev_status  = get_sub_field('sub_category_dev_status');
						?>
						<li>
							<a href="#<?=$subcat_slug?>">
								<?=$subcat_title;?>

								<?php /* Label with development status of the feature. */ ?>
								<?php if ($subcat_dev_status != 'implemented'): ?>
									<span class="cards-list-item-label <?=$subcat_dev_status?>" title="<?=$dev_status[$subcat_dev_status]['desc']?>">
										<?=$dev_status[$subcat_dev_status]['title']?>
									</span>
								<?php endif; ?>

							</a>
							<?php if ($subcat_details): ?>
								<ul class="nav-sub">
								<?php while( have_rows('feature_details') ): the_row();
									$detail_title = get_sub_field('detail_title');
									$detail_slug = slugify($detail_title);
									$detail_dev_status = get_sub_field('detail_dev_status');
								?>

									<li>
										<a class="nav-detail-title" href="#<?=$subcat_slug?>-<?=$detail_slug?>">
											<?=$detail_title;?>

											<?php /* Label with development status of the feature. */ ?>
											<?php if ($detail_dev_status != 'implemented'): ?>
												<span class="cards-list-item-label <?=$detail_dev_status?>" title="<?=$dev_status[$detail_dev_status]['desc']?>">
													<?=$dev_status[$detail_dev_status]['title']?>
												</span>
											<?php endif; ?>

										</a>
									</li>

								<?php endwhile; //feature_details ?>
								</ul>
							<?php endif; // subcat_details ?>
						</li>
						<?php endwhile; // sub_categories ?>
					</ul>
				</nav>
			</div>
		<?php endif; // sub_categories ?>


		<?php /* LIST OF FEATURES*/ ?>
		<div class="<?=($sub_categories ? 'col-sm-8' : 'col-md-10 offset-md-1')?>">
			<?php if ($sub_categories): ?>
				<div class="cards-list features-category">
					<?php while( have_rows('sub_categories') ): the_row();
						$subcat_title       = get_sub_field('sub_category_title');
						$subcat_slug        = slugify($subcat_title);
						$subcat_description = get_sub_field('sub_category_description');
						$subcat_image       = get_sub_field('sub_category_image');
						$subcat_video       = get_sub_field('sub_category_video');
						$subcat_url         = get_sub_field('sub_category_url');
						$subcat_dev_status  = get_sub_field('sub_category_dev_status');

						$subcat_details     = get_sub_field('feature_details'); // List of sub-category features
					?>

					<div id="<?=$subcat_slug?>" class="cards-list-item-outer subcat">
						<div class="cards-list-item-inner">
							<?php if ($subcat_image): ?>
								<div class="cards-list-item-thumbnail js-isolify" data-url="<?=$subcat_image['url']?>">
									<img src="<?=$subcat_image['url']?>" alt="<?=$subcat_title;?>"/>
								</div>
							<?php endif; ?>
							<span class="cards-list-item-title">
								<a href="#<?=$subcat_slug?>"><?=$subcat_title;?></a>

								<?php /* Label with development status of the feature. */ ?>
								<?php if ($subcat_dev_status != 'implemented'): ?>
									<span class="cards-list-item-label" title="<?=$dev_status[$subcat_dev_status]['desc']?>">
										<?=$dev_status[$subcat_dev_status]['title']?>
									</span>
								<?php endif; ?>

							</span>
							<div class="cards-list-item-description">
								<?=$subcat_description;?>
							</div>
							<?php if ($subcat_url or $subcat_video): ?>
							<div class="cards-list-item-more">
								<?php if ($subcat_video): ?>
									<div class="more-item video js-isolify" data-type="oembed" data-url="<div class='embed-container'><?=htmlspecialchars($subcat_video)?></div>"><i class="i-play"></i> WATCH VIDEO</div>
								<?php endif; ?>
								<?php if ($subcat_url): ?>
									<a class="more-item url" href="<?=$subcat_url?>" target="_blank">READ MORE  <i class="i-chevron-right"></i></a>
								<?php endif; ?>
							</div>
							<?php endif; ?>
						</div>
					</div>

						<?php if ($subcat_details): ?>
							<?php while( have_rows('feature_details') ): the_row();
								$detail_title       = get_sub_field('detail_title');
								$detail_slug        = slugify($detail_title);
								$detail_description = get_sub_field('detail_description');
								$detail_image       = get_sub_field('detail_image');
								$detail_url         = get_sub_field('detail_url');
								$detail_video       = get_sub_field('detail_video');
								$detail_dev_status  = get_sub_field('detail_dev_status');
							?>

							<div class="cards-list-item-outer">
								<div class="cards-list-item-inner">
									<a id="<?=$subcat_slug?>-<?=$detail_slug?>" name="<?=$subcat_slug?>-<?=$detail_slug?>"></a>
									<?php if ($detail_image): ?>
										<div class="cards-list-item-thumbnail js-isolify" data-url="<?=$detail_image['url']?>">
											<img src="<?=$detail_image['url']?>" alt="<?=$detail_title;?>"/>
										</div>
									<?php endif; ?>
									<div class="cards-list-item-title">
										<span class="cards-list-item-pretitle">
											<a href="#<?=$subcat_slug?>"><?=$subcat_title;?></a> <i class="i-chevron-right"></i>
										</span>
										<a href="#<?=$subcat_slug?>-<?=$detail_slug?>"><?=$detail_title;?></a>

										<?php /* Label with development status of the feature. */ ?>
										<?php if ($detail_dev_status != 'implemented'): ?>
											<span class="cards-list-item-label" title="<?=$dev_status[$detail_dev_status]['desc']?>">
												<?=$dev_status[$detail_dev_status]['title']?>
											</span>
										<?php endif; ?>

									</div>
									<div class="cards-list-item-description">
										<?=$detail_description;?>
									</div>
									<?php if ($detail_url or $detail_video): ?>
									<div class="cards-list-item-more">
										<?php if ($detail_video): ?>
											<div class="more-item video js-isolify" data-type="oembed" data-url="<?=htmlspecialchars($detail_video)?>"><i class="i-play"></i> WATCH VIDEO</div>
										<?php endif; ?>
										<?php if ($detail_url): ?>
											<a class="more-item url" href="<?=$detail_url?>" target="_blank">READ MORE  <i class="i-chevron-right"></i></a>
										<?php endif; ?>
									</div>
									<?php endif; ?>
								</div>
							</div>
							<?php endwhile; //feature_details ?>
						<?php endif; // subcat_details ?>
					<?php endwhile; // sub_categories ?>
				</div>
			<?php endif; // sub_categories ?>

		</div>
	</div>
</div>

<script>
	document.addEventListener("DOMContentLoaded", function(event) {

		var offsetTop = $('.navbar-secondary').offset().top + 40;
		$('#features-nav').data('offset-top', offsetTop);

		$('body').scrollspy({
			target: '#features-nav',
			top: offsetTop
		});
	});
</script>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
