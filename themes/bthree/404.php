
<?php get_header(); ?>
<?php $phrases = array(
	"What do you see at your left side, Emo?" => "https://youtu.be/TLkA0RELQ1g?t=359",
	"...the colossus of Rhodes!" => "https://youtu.be/TLkA0RELQ1g?t=493",
	"You forgot me on Earth." => "https://youtu.be/R6MlUcmOul8?t=387",
	"We already tried that one!" => "https://youtu.be/R6MlUcmOul8?t=366",
	"I just want to be awesome in space." => "https://youtu.be/R6MlUcmOul8?t=28",
	"I'm searching for someone." => "https://youtu.be/eRsGyueVLvQ?t=135",
	"You're closer than you know." => "https://youtu.be/eRsGyueVLvQ?t=460",
	"You are so much more than just a sheep, aren't you?" => "https://youtu.be/Y-rmzh0PI3c?t=181",
	"Took me ages to tie that rope." => "https://youtu.be/Y-rmzh0PI3c?t=301",
	"Wait wait wait wait wait wait." => "https://youtu.be/Y-rmzh0PI3c?t=315",
	"I just wanted a haircut." => "https://youtu.be/mN0zPOpADL4?t=166",
	"Is it usually this... empty?" => "https://youtu.be/mN0zPOpADL4?t=60",
	"You picked the wrong camping spot." => "https://youtu.be/_cMxraX_5RE?t=299",
	"Could I interest you in some peppermint tea?" => "https://youtu.be/_cMxraX_5RE?t=205"
);
?>
<div class="container py-3">
  <div class="row">
    <div class="col-md-8 mx-auto py-5">
      <div class="box p-5 text-center">
        <h2 class="mx-auto w-75">
          <?php $which = rand(0, (count($phrases) - 1)); ?>
            <a href="<?=array_values($phrases)[$which]?>" target="_blank">
              <?php echo('"' . array_keys($phrases)[$which] . '"'); ?>
            </a>
        </h2>
        <p class="pt-3">Whatever you were looking for is no longer here <span class="text-muted">(404)</span></p>
        <hr class="my-5"/>
        <ul class="list-unstyled d-flex list-bullets-none mx-auto justify-content-center">
          <li>
            <a href="https://youtube.com/BlenderFoundation" title="YouTube" class="d-block">
              <i class="i-youtube h1"></i>
            </a>
          </li>
          <li>
            <a href="https://twitter.com/blender" title="X" class="d-block">
              <i class="i-twitter h1"></i>
            </a>
          </li>
          <li>
            <a href="https://instagram.com/blender.official/" title="Instagram" class="d-block">
              <i class="i-instagram h1"></i>
            </a>
          </li>
          <li>
            <a href="https://www.facebook.com/YourOwn3DSoftware/" title="Facebook" class="d-block">
              <i class="i-facebook h1"></i>
            </a>
          </li>
          <li>
            <a href="https://www.linkedin.com/company/blender-org/" title="LinkedIn" class="d-block">
              <i class="i-linkedin h1"></i>
            </a>
          </li>
          <li style="display:flex;align-items:center;">
            <a href="https://mastodon.social/@blender" title="Mastodon" class="d-block">
              <svg style="stroke:currentColor;stroke-width:.8rem;height:2rem;width:2rem;margin:0 .5rem;" xmlns="http://www.w3.org/2000/svg" width="61.076954mm" height="65.47831mm" viewBox="0 0 216.4144 232.00976">
                <path d="M 107.87112,1.8691521 C 78.687821,2.1077446 50.615693,5.2679473 34.256124,12.781149 c 0,0 -32.4459422,14.513545 -32.4459422,64.032543 0,11.33929 -0.220399,24.897358 0.1387195,39.275628 1.1782038,48.42688 8.8777977,96.15389 53.6520047,108.00481 20.644395,5.46425 38.369975,6.60907 52.644934,5.82442 25.88728,-1.43526 40.41961,-9.23836 40.41961,-9.23836 l -0.85394,-18.78298 c 0,0 -18.49963,5.83267 -39.27562,5.12182 -20.58413,-0.70594 -42.314881,-2.21933 -45.644106,-27.49168 -0.307465,-2.2199 -0.461196,-4.59439 -0.461196,-7.0873 0,0 20.207151,4.93937 45.815252,6.11266 15.65855,0.71823 30.34229,-0.91732 45.25677,-2.69692 28.60157,-3.41533 53.50539,-21.03826 56.63538,-37.14079 4.93172,-25.36583 4.52549,-61.901308 4.52549,-61.901308 0,-49.518998 -32.44414,-64.032543 -32.44414,-64.032543 C 165.861,5.2679473 137.77144,2.1077446 108.58813,1.8691521 Z M 74.843276,40.561072 c 12.155915,0 21.360069,4.672182 27.446624,14.017872 l 5.91811,9.919343 5.91809,-9.919343 c 6.08534,-9.34569 15.28949,-14.017872 27.44664,-14.017872 10.50545,0 18.97017,3.693077 25.43431,10.897586 6.26612,7.204507 9.38608,16.94344 9.38608,29.197743 V 140.61564 H 152.63788 V 82.418319 c 0,-12.267833 -5.16149,-18.494728 -15.48615,-18.494728 -11.41555,0 -17.13636,7.385747 -17.13636,21.99154 V 117.77016 H 96.400641 V 85.915131 c 0,-14.605793 -5.722618,-21.99154 -17.138159,-21.99154 -10.324658,0 -15.486137,6.226895 -15.486137,18.494728 V 140.61564 H 40.021088 V 80.656401 c 0,-12.254303 3.120529,-21.993236 9.387885,-29.197743 C 55.871876,44.254149 64.3366,40.561072 74.843276,40.561072 Z" style="fill:none;stroke-linecap:square;stroke-linejoin:round;stroke-dasharray:none;stroke-opacity:1;paint-order:normal"></path>
              </svg>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
<style>
.navbar-secondary {
  display: none;
}
</style>
