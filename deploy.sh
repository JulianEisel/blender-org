#!/bin/bash -e

case $1 in
    *blender.org*)
        DEPLOYHOST="$1"
        ;;
    *)
        echo "Usage:" >&2
        echo "$0 blender.org" >&2
        echo "$0 code.blender.org" >&2
        exit 1
esac

echo -n "Deploying to ${DEPLOYHOST}"
echo

if ! ping blender.org -q -c 1 -W 2 >/dev/null; then
    echo "host ${DEPLOYHOST} cannot be pinged, refusing to deploy." >&2
    exit 2
fi

echo "Press [ENTER] to continue, Ctrl+C to abort."
read dummy


# macOS does not support readlink -f, so we use greadlink instead
if [[ `uname` == 'Darwin' ]]; then
    command -v greadlink 2>/dev/null 2>&1 || { echo >&2 "Install greadlink using brew."; exit 1; }
    readlink='greadlink'
else
    readlink='readlink'
fi

ROOT="$(dirname "$($readlink -f "$0")")"
cd ${ROOT}

CONTENT="$ROOT/"

if [ ! -d "$CONTENT" ]; then
    echo "Unable to find dir $CONTENT"
    exit 1
fi


echo
echo "*** GULPA GULPA ***"
npm install
npm run build

echo
echo "*** SYNCING $1 ***"

if [[ "$1" == 'blender.org' ]]; then
    rsync -avh --filter="merge deploy-filter.txt" $CONTENT blender.org:/data/www/vhosts/www.blender.org/www/wp-content/
fi

if [[ "$1" == 'code.blender.org' ]]; then
    rsync -avh --filter="merge deploy-filter.txt" $CONTENT blender.org:/data/www/vhosts/code.blender.org/wordpress/wp-content/
fi

echo
echo "==================================================================="
echo "Deploy to ${DEPLOYHOST} is done."
echo "==================================================================="
